<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<c:url var="editUser" value="/admin-user-edit"/>
<html>
<head>
    <title>Chỉnh sửa người dùng</title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Trang chủ</a>
                </li>
                <li class="active">Chỉnh sửa người dùng</li>
            </ul><!-- /.breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <c:if test="${not empty messageResponse}">
                        <div class="alert alert-block alert-${alert}">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                                ${messageResponse}
                        </div>
                    </c:if>
                    <form id="formEdit" method="POST" action="${editUser}">
                    	<div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Vai trò</label>
                            <div class="col-sm-9">
                                <select name="roleCode" >
                                    <c:if test="${empty model.roleCode}">
                                        <option value="">Chọn vai trò</option>
                                        <c:forEach var="item" items="${roles}">
                                            <option value="${item.code}">${item.name}</option>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${not empty model.roleCode}">
                                        <c:forEach var="item" items="${roles}">
                                            <option value="${item.code}" <c:if test="${item.code eq model.roleCode}">selected="selected"</c:if>>
                                                    ${item.name}
                                            </option>
                                        </c:forEach>
                                        <option value="">Chọn vai trò</option>
                                    </c:if>
                                </select>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Tên người dùng</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" value="${model.name}"/>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Mật khẩu người dùng</label>
                            <div class="col-sm-9">
                                <input type="password" name="password" value="${model.password}"/>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Tên đầy đủ</label>
                            <div class="col-sm-9">
                                <input type="text" name="fullName" value="${model.fullName}"/>
                            </div>
                        </div>
                        <br/>
                        <br/>                                                                
                        <div class="form-group">
                            <div class="col-sm-12">
                            	<c:if test="${not empty model.id}">
                                    <input type="hidden" name="id" value="${model.id}"/>
                        			<input type="submit" class="btn btn-white btn-warning btn-bold" value="Cập nhật người dùng" id="btnUpdateUser"/>
                        		</c:if>
                        		<c:if test="${empty model.id}">
                        			<input type="submit" class="btn btn-white btn-warning btn-bold" value="Thêm mới người dùng" id="btnAddUser"/>
                        		</c:if>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function () {

	});
</script>
</body>
</html>
