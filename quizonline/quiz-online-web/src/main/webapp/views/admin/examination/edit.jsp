<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<c:url var="editExamination" value="/admin-examination-edit" />
<html>
<head>
<title>Chỉnh sửa bài thi</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>
				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
							chủ</a></li>
					<li class="active">Chỉnh sửa bài thi</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${param.result == '1'}">
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Thêm bài thi thành công
							</div>
						</c:if>
						<c:if test="${param.result == '2'}">
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Cập nhật bài thi thành công
							</div>
						</c:if>
						<form id="formEdit" method="POST" action="${editExamination}">
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Loại
									đề thi</label>
								<div class="col-sm-9">
									<select id="selectedCategory" onchange="selectOnchange()" name="categoryCodeSelected">
										<c:if test="${empty model.category.code}">
											<option value="">Chọn loại đề thi</option>
											<c:forEach var="item" items="${categories}">
												<option data-levels="${item.levels}" value="${item.code}">${item.name}</option>
											</c:forEach>
										</c:if>
										<c:if test="${not empty model.category.code}">
											<c:forEach var="item" items="${categories}">
												<option data-levels="${item.levels}" value="${item.code}"
													<c:if test="${item.code eq model.category.code}">selected="selected"</c:if>>
													${item.name}</option>
											</c:forEach>
											<option value="">Chọn loại đề thi</option>
										</c:if>
									</select>
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Cấp
									độ</label>
								<div class="col-sm-9">
									<select id="selectLevel" name="levelCodeSelected">
										<c:if test="${empty model.level.code}">
											<option value="">Chọn cấp độ</option>
											<c:forEach var="item" items="${levels}">
												<option id="level${item.id}"  value="${item.code}">${item.name}</option>
											</c:forEach>
										</c:if>
										<c:if test="${not empty model.level.code}">
											<c:forEach var="item" items="${levels}">
												<option id="level${item.id}"  value="${item.code}"
													<c:if test="${item.code eq model.level.code}">selected="selected"</c:if>>
													${item.name}</option>
											</c:forEach>
											<option value="">Chọn cấp độ</option>
										</c:if>
									</select>
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Tên
									đề thi</label>
								<div class="col-sm-9">
									<input type="text" name="name" value="${model.name}" />
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Thời
									gian thi</label>
								<div class="col-sm-9">
									<input type="text" name="time" value="${model.time}" />
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Số
									lần thi tối đa của đề thi</label>
								<div class="col-sm-9">
									<input type="text" name="maxIteration"
										value="${model.maxIteration}" />
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Mã
									đề thi</label>
								<div class="col-sm-9">
									<input type="text" name="code" value="${model.code}" />
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<div class="col-sm-12">

									<c:if test="${not empty model.id}">
										<input type="hidden" name="id" value="${model.id}" />
										<input type="submit"
											class="btn btn-white btn-warning btn-bold"
											value="Cập nhật đề thi" id="btnUpdateExamination" />
									</c:if>
									<c:if test="${empty model.id}">
										<input type="submit"
											class="btn btn-white btn-warning btn-bold"
											value="Thêm mới đề thi" id="btnAddExamination" />
									</c:if>


								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	function selectOnchange() {
		var test  = $("#selectedCategory").find(':selected').attr(
				'data-levels');
		console.log(test+"");
		var level = JSON.parse("[ " + test + " ]");
		var id = $(this).attr("id");
		$("#selectLevel option").each(function() {
			var idCheck = $(this).attr("id");
			$("#" + idCheck).hide();
		});

		for (var m = 0; m < level.length; m++) {
			var idLevel = "level" + level[m];
			$("#" + idLevel).show();
		}
		
		$('#selectLevel').prop('selectedIndex',0);
	}

    $(document).ready(function () {
    	selectOnchange();
    });
</script>
</body>
</html>
