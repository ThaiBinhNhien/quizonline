<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/common/taglib.jsp"%>
<c:url var="listExaminationUrl" value="/admin-examination-list" />
<html>
<head>
<title>Danh sách đề thi</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>

				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
							chủ</a></li>
					<li class="active">Danh sách đề thi</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${param.result == '1'}">
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Xóa bài viết bài thi thành công
							</div>
						</c:if>
						<c:if test="${param.result == '2'}">
							<div class="alert alert-block alert-danger">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Xóa bài viết bài thi thất bại
							</div>
						</c:if>
						<form action="${listExaminationUrl}" method="get" id="formUrl">
							<div class="row">
								<div class="col-xs-12">
									<div class="widget-box table-filter">
										<div class="widget-header">
											<h4 class="widget-title">Tìm kiếm</h4>
											<div class="widget-toolbar">
												<a href="#" data-action="collapse"> <i
													class="ace-icon fa fa-chevron-up"></i>
												</a>
											</div>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="form-horizontal">
													<div class="form-group">
														<label class="col-sm-2 control-label">Tên đề thi</label>
														<div class="col-sm-8">
															<div class="fg-line">
																<input type="text" value="${model.name}"
																	class="form-control input-sm" name="name" />
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label">Loại bài thi</label>
														<div class="col-sm-8">
															<div class="fg-line">
																<select onchange="selectOnchange()"
																	id="selectedCategory" name="categoryCode">
																	<c:if test="${empty categoryCode}">
																		<option value="">Chọn loại đề thi</option>
																		<c:forEach var="item" items="${categories}">
																			<option data-levels="${item.levels}"
																				value="${item.code}">${item.name}</option>
																		</c:forEach>
																	</c:if>
																	<c:if test="${not empty categoryCode}">
																		<c:forEach var="item" items="${categories}">
																			<option data-levels="${item.levels}"
																				value="${item.code}"
																				<c:if test="${item.code eq categoryCode}">selected="selected"</c:if>>
																				${item.name}</option>
																		</c:forEach>
																		<option value="">Chọn loại đề thi</option>
																	</c:if>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label">Cấp độ đề
															thi</label>
														<div class="col-sm-8">
															<div class="fg-line">
																<select id="selectLevel" name="levelCode">
																	<c:if test="${empty levelCode}">
																		<option value="">Chọn cấp độ</option>
																		<c:forEach var="item" items="${levels}">
																			<option id="level${item.id}" value="${item.code}">${item.name}</option>
																		</c:forEach>
																	</c:if>
																	<c:if test="${not empty levelCode}">
																		<c:forEach var="item" items="${levels}">
																			<option id="level${item.id}" value="${item.code}"
																				<c:if test="${item.code eq levelCode}">selected="selected"</c:if>>
																				${item.name}</option>
																		</c:forEach>
																		<option value="">Chọn cấp độ</option>
																	</c:if>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label"></label>
														<div class="col-sm-8">
															<button id="btnSearch" class="btn btn-sm btn-success">
																Tìm kiếm <i
																	class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="table-btn-controls">
										<div class="pull-right tableTools-container">
											<div class="dt-buttons btn-overlap btn-group">
												<c:url var="insertExamination"
													value="/admin-examination-edit">
													<c:param name="type" value="edit" />
												</c:url>
												<a flag="info"
													class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
													href="${insertExamination}"> <span> <i
														class="fa fa-plus-circle bigger-110 purple"></i>
												</span>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="table-responsive">
								<display:table id="tableList" name="model.listResult"
									partialList="true" size="${model.totalItems}"
									pagesize="${model.maxPageItems}" sort="external"
									requestURI="${listExaminationUrl}"
									class="table table-fcv-ace table-striped table-bordered table-hover dataTable no-footer"
									style="margin: 3em 0 1.5em;">
									<display:column property="name" title="Tên đề thi" />
									<display:column property="category.name" title="Loại đề thi" />
									<display:column property="level.name" title="Cấp độ đề thi" />
									<display:column property="createdBy" title="Tạo bởi" />
									<display:column headerClass="col-actions" title="Thao tác">
										<c:url var="editUrl" value="/admin-examination-edit">
											<c:param name="type" value="edit" />
											<c:param name="id" value="${tableList.id}" />
										</c:url>
										<a class="btn btn-sm btn-primary btn-edit" href="${editUrl}"
											data-toggle="tooltip" title="Cập nhật đề thi"><i
											class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
										<c:url var="examinationQuestions"
											value="/admin-examination-question-list?id=${tableList.id}" />
										<a class="btn btn-sm btn-primary btn-edit"
											href="${examinationQuestions}" data-toggle="tooltip"
											title="Danh sách câu hỏi đề thi"><i class="fa fa-list"
											aria-hidden="true"></i></a>
										<c:url var="deleteUrl" value="/admin-examination-edit">
											<c:param name="type" value="delete" />
											<c:param name="id" value="${tableList.id}" />
										</c:url>
										<a class="btn btn-sm btn-danger btn-cancel"
											href="${deleteUrl}" data-toggle="tooltip" title="Xóa đề thi"><i
											class="fa fa-trash" aria-hidden="true"></i></a>
									</display:column>
								</display:table>
							</div>
							<input type="hidden" name="type" value="list" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		function selectOnchange() {
			var test  = $("#selectedCategory").find(':selected').attr(
					'data-levels');
			var level = JSON.parse("[ " + test + " ]");
			var id = $(this).attr("id");
			$("#selectLevel option").each(function() {
				var idCheck = $(this).attr("id");
				$("#" + idCheck).hide();
			});

			for (var m = 0; m < level.length; m++) {
				var idLevel = "level" + level[m];
				$("#" + idLevel).show();
			}
			
			$('#selectLevel').prop('selectedIndex',0);
		}

		$(document).ready(function() {

			selectOnchange();

			$('#btnSearch').click(function(e) {
				e.preventDefault();
				$('#formUrl').submit();
			});
		});
	</script>
</body>
</html>
