<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<c:url var="requestUrl" value="/admin-examination-question-list" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quản lý câu hỏi đề thi</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Quản
							lý câu hỏi đề thi</a></li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${param.result == '1'}">
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Xóa bài viết bài thi thành công
							</div>
						</c:if>
						<c:if test="${param.result == '2'}">
							<div class="alert alert-block alert-danger">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Xóa bài viết bài thi thất bại
							</div>
						</c:if>
						<div class="widget-box table-filter">
							<div class="widget-header">
								<h4 class="widget-title">Tìm kiếm</h4>
								<div class="widget-toolbar">
									<a href="#" data-action="collapse"> <i
										class="ace-icon fa fa-chevron-up"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<div class="form-horizontal">
										<c:url var="searchQuestion"
											value="/admin-examination-question-list" />
										<form action="${searchQuestion}" method="GET" id="formSearch">
											<div class="form-group">
												<label class="col-sm-2 control-label">Giá trị cần
													tìm</label>
												<div class="col-sm-8">
													<div class="fg-line">
														<input type="text" value="${model.searchValue}"
															class="form-control input-sm" name="searchValue" />
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"></label>
												<div class="col-sm-8">
													<button id="btnSearch" class="btn btn-sm btn-success">
														Tìm kiếm <i
															class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
													</button>
												</div>
											</div>
											<input type="hidden" name="id" value="${examinationId}" />
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="table-btn-controls">
							<div class="pull-right tableTools-container">
								<div class="dt-buttons btn-overlap btn-group">
									<c:url var="importUrl"
										value="/admin-examination-question-import">
										<c:param name="id" value="${examinationId}" />
										<c:param name="type" value="import-question-page" />
									</c:url>
									<a flag="info"
										class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
										href="${importUrl}" data-toggle="tooltip"
										title="Import câu hỏi từ excel"> <span> <i
											class="fa fa-plus-circle bigger-110 purple"></i>
									</span>
									</a>
									<c:url var="exportUrl"
										value="/admin-export-examination-question">
										<c:param name="id" value="${examinationId}" />
									</c:url>
									<a flag="info"
										class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
										href="${exportUrl}" data-toggle="tooltip"
										title="Export câu hỏi đề thi"> <span> <i
											class="fa fa-print bigger-110 purple"></i>
									</span>
									</a>
									<c:if test="${not empty link_download_export_examination}">
										<a flag="info"
											class="dt-button buttons-colvis btn btn-white btn-primary btn-bold"
											href="<c:url value="/repository/${link_download_export_examination}"/>"
											data-toggle="tooltip" title="Tải file"> <span> <i
												class="fa fa-download"></i>
										</span>
										</a>
									</c:if>
									<c:url var="deleteUrl" value="/admin-examination-question-list">
										<c:param name="id" value="${examinationId}" />
										<c:param name="type" value="deleteall" />
									</c:url>
									<a flag="info"
										class="dt-button buttons-html5 btn btn-white btn-primary btn-bold"
										href="${deleteUrl}" data-toggle="tooltip"
										title="Xóa tất cả câu hỏi trong danh sách"> <span> <i
											class="fa fa-trash-o bigger-110 pink"></i>
									</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<display:table id="tableList" name="model.listResult"
						partialList="true" size="${model.totalItems}"
						pagesize="${model.maxPageItems}" sort="external"
						requestURI="${requestUrl}?id=${examinationId}"
						class="table table-fcv-ace table-striped table-bordered table-hover dataTable no-footer"
						style="margin: 3em 0 1.5em;">
						<display:column property="question" titleKey="Câu hỏi" />
						<display:column property="option1" titleKey="Câu hỏi 1" />
						<display:column property="option2" titleKey="Câu hỏi 2" />
						<display:column property="option3" titleKey="Câu hỏi 3" />
						<display:column property="option4" titleKey="Câu hỏi 4" />
						<display:column property="correctAnswer" titleKey="Đáp án đúng" />
					</display:table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.main-content -->
	<script>
    $(document).ready(function () {
        $('#btnSearch').click(function (e) {
            e.preventDefault();
            $('#formSearch').submit();
        });
    });
</script>
</body>
</html>