<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/common/taglib.jsp"%>
<c:url var="export" value="/admin-export-user-result"/>
<c:url var="listResultUrl" value="/admin-user-result"/>
<html>
<head>
    <title>Quản lý kết quả thi của user</title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Trang chủ</a>
                </li>
                <li class="active">Quản lý kết quả thi của user</li>
            </ul><!-- /.breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <c:if test="${not empty messageResponse}">
                        <div class="alert alert-block alert-${alert}">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                                ${messageResponse}
                        </div>
                    </c:if>
                    <form action="" method="get" id="formUrl">
                        <div class="row">
                            <div class="col-xs-12">
								<div class="widget-box table-filter">
									<div class="widget-header">
										<h4 class="widget-title">Lựa chọn</h4>
										<div class="widget-toolbar">
											<a href="#" data-action="collapse">
												<i class="ace-icon fa fa-chevron-up"></i>
											</a>
										</div>
									</div>
									<div class="widget-body">
										<div class="widget-main">
											<div class="form-horizontal">
												<div class="form-group">
													<label class="col-sm-2 control-label">Chọn đề thi</label>
													<div class="col-sm-8">
														<select id="examination" name="examinationCode" class="form-control input-sm">
															<option value="">Chọn đề thi</option>
															<c:forEach var="item" items="${model.examinations}">
																<option value="${item.code}">${item.name}</option>
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Chọn người thi</label>
													<div class="col-sm-8">
														<select id="names" name="names" class="form-control input-sm" multiple style="height: 80px">
															<option value="">Chọn người thi</option>
															<c:forEach var="item" items="${model.users}">
																<option value="${item.name}">${item.name}</option>
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Ngày bắt đầu</label>
													<div class="col-sm-8">
														<input type="text" id="fromDate" class="form-control input-sm" name="fromDate">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Ngày kết thúc</label>
													<div class="col-sm-8">
														<input type="text" id="toDate" class="form-control input-sm" name="toDate">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
                            	<div class="table-btn-controls">
	                                    <div class="pull-right tableTools-container">
	                                        <div class="dt-buttons btn-overlap btn-group">
	                                            <a flag="info" class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" id="exportExcel" data-toggle="tooltip" title="Xuất kết quả thi">
	                                                    <span>
	                                                        <i class="fa fa-print bigger-110 purple"></i>
	                                                    </span>
	                                            </a>
												<c:if test="${not empty link_download_export_user_result}">
													<a flag="info" class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" href="<c:url value="/repository/${link_download_export_user_result}"/>" data-toggle="tooltip" title="Tải file">
	                                                    <span>
	                                                        <i class="fa fa-download"></i>
	                                                    </span>
													</a>
												</c:if>
	                                        </div>
	                                    </div>
                    			</div>
								<div class="table-responsive">
									<display:table id="tableList" name="model.listResult" partialList="true" size="${model.totalItems}"
												   pagesize="${model.maxPageItems}" defaultsort="1" sort="external" requestURI="${listResultUrl}"
												   class="table table-fcv-ace table-striped table-bordered table-hover dataTable no-footer"
												   style="margin: 3em 0 1.5em;">
										<display:column property="examination.name" title="Đề thi"/>
										<display:column property="correctNumber" title="Số câu đúng" sortable="true"/>
										<display:column property="totalNumber" title="Tổng số câu" sortable="true"/>
										<display:column property="user.name" title="Tên người làm" sortable="true"/>
										<display:column property="createdDate" title="Ngày làm bài thi" sortable="true"/>
									</display:table>
								</div>
                            </div>
                        </div>                  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $( "#fromDate" ).datepicker();
        $( "#toDate" ).datepicker();
    	$("#exportExcel").click(function(){
    		$("#formUrl").attr('action', '${export}');
            $("#formUrl").submit();
        });
    });
</script>
</body>
</html>
