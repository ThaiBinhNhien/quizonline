<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<c:url var="editLevel" value="/admin-level-edit"/>
<html>
<head>
    <title>Chỉnh sửa lớp thi</title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Trang chủ</a>
                </li>
                <li class="active">Chỉnh sửa lớp thi</li>
            </ul><!-- /.breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <c:if test="${not empty messageResponse}">
                        <div class="alert alert-block alert-${alert}">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                                ${messageResponse}
                        </div>
                    </c:if>
                    <form id="formEdit" method="POST" action="${editLevel}">
                        <br/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Tên lớp thi</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" value="${model.name}"/>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right">Mã lớp thi</label>
                            <div class="col-sm-9">
                                <input type="text" name="code" value="${model.code}"/>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                            <div class="col-sm-12">
                            	<c:if test="${not empty model.id}">
                                    <input type="hidden" name="id" value="${model.id}"/>
                        			<input type="submit" class="btn btn-white btn-warning btn-bold" value="Cập nhật lớp thi" id="btnUpdateUser"/>
                        		</c:if>
                        		<c:if test="${empty model.id}">
                        			<input type="submit" class="btn btn-white btn-warning btn-bold" value="Thêm mới lớp thi" id="btnAddUser"/>
                        		</c:if>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function () {

	});
</script>
</body>
</html>
