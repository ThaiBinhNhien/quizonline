<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/common/taglib.jsp"%>
<c:url var="listLevelUrl" value="/admin-level-list">
    <c:param name="type" value="list"/>
</c:url>
<html>
<head>
    <title>Danh sách lớp thi</title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Trang chủ</a>
                </li>
                <li class="active">Danh sách lớp thi</li>
            </ul><!-- /.breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <c:if test="${param.result == '1'}">
							<div class="alert alert-block alert-success">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Xóa bài viết lớp thi thành công
							</div>
						</c:if>
						<c:if test="${param.result == '2'}">
							<div class="alert alert-block alert-danger">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								Xóa bài viết lớp thi thất bại
							</div>
						</c:if>
                    <form action="${listLevelUrl}" method="get" id="formUrl">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-btn-controls">
                                    <div class="pull-right tableTools-container">
                                        <div class="dt-buttons btn-overlap btn-group">
                                            <c:url var="insertLevel" value="/admin-level-edit">
                                                <c:param name="type" value="edit"/>
                                            </c:url>
                                            <a flag="info" class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" href="${insertLevel}">
                                                <span>
                                                    <i class="fa fa-plus-circle bigger-110 purple"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <display:table id="tableList" name="model.listResult" partialList="true" size="${model.totalItems}"
                                           pagesize="${model.maxPageItems}" sort="external" requestURI="${listCategoriesUrl}"
                                           class="table table-fcv-ace table-striped table-bordered table-hover dataTable no-footer"
                                           style="margin: 3em 0 1.5em;">
                                <display:column property="name" title="Tên môn"/>
                                <display:column property="code" title="Mã môn"/>
                                <display:column headerClass="col-actions" title="Thao tác">
                                    <c:url var="editUrl" value="/admin-level-edit">
                                        <c:param name="type" value="edit"/>
                                        <c:param name="id" value="${tableList.id}"/>
                                    </c:url>
                                    <a class="btn btn-sm btn-primary btn-edit" href="${editUrl}" data-toggle="tooltip" title="Cập nhật lớp thi"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <c:url var="deleteUrl" value="/admin-level-edit">
                                        <c:param name="type" value="delete"/>
                                        <c:param name="id" value="${tableList.id}"/>
                                    </c:url>
                                    <a class="btn btn-sm btn-danger btn-cancel" href="${deleteUrl}" data-toggle="tooltip" title="Xóa lớp thi"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </display:column>
                            </display:table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

    });
</script>
</body>
</html>
