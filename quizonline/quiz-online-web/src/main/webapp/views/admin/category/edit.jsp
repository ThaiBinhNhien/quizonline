<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/common/taglib.jsp"%>
<c:url var="editCategories" value="/admin-categories-edit" />
<html>
<head>
<title>Chỉnh sửa môn thi</title>
</head>
<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch (e) {
					}
				</script>
				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Trang
							chủ</a></li>
					<li class="active">Chỉnh sửa môn thi</li>
				</ul>
				<!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
						<c:if test="${not empty messageResponse}">
							<div class="alert alert-block alert-${alert}">
								<button type="button" class="close" data-dismiss="alert">
									<i class="ace-icon fa fa-times"></i>
								</button>
								${messageResponse}
							</div>
						</c:if>
						<form id="formEdit" method="POST" action="${editCategories}">
							<br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Tên
									môn thi</label>
								<div class="col-sm-9">
									<input type="text" name="name" value="${model.name}" />
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Mã
									môn thi</label>
								<div class="col-sm-9">
									<input type="text" name="code" value="${model.code}" />
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right">Lớp
									của môn</label>
								<div class="col-sm-9 checkbox">
									<input id="idLevels" type="text" name="levels" value="${model.levels}"
										hidden=true />
									<c:forEach var="level" items="${levels}" varStatus="loop">
										<label class="checkbox-inline"> <input
											id="checkbox${level.id}" type="checkbox"
											onchange="funcOnchange(${level.id})" name="levels"
											value="${level.id}">${level.name}</label>
										<c:if test="${loop.index % 3 == 0}">
											<br />
											<br />
										</c:if>
									</c:forEach>
								</div>
							</div>
							<br /> <br />
							<div class="form-group">
								<div class="col-sm-12">
									<c:if test="${not empty model.id}">
										<input type="hidden" name="id" value="${model.id}" />
										<input type="submit"
											class="btn btn-white btn-warning btn-bold"
											value="Cập nhật môn thi" id="btnUpdateUser" />
									</c:if>
									<c:if test="${empty model.id}">
										<input type="submit"
											class="btn btn-white btn-warning btn-bold"
											value="Thêm mới môn thi" id="btnAddUser" />
									</c:if>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
			var traingIds = $("#idLevels").val();
			listIdLevel = JSON.parse("[" + traingIds + " ]");
			for(var i = 0 ; i < listIdLevel.length ; i++){
				$('#checkbox'+listIdLevel[i]).prop('checked', true);
			}
			console.log(listIdLevel);
			function funcOnchange (id){
				if (document.getElementById('checkbox'+id).checked) 
				{
					console.log("unchecked");
					listIdLevel.push(id);
				} else {
					var index = listIdLevel.indexOf(id);
					if (index !== -1) listIdLevel.splice(index, 1);
					console.log("checked");
				}
				console.log(listIdLevel);
				$("#idLevels").val(listIdLevel);
			}
	</script>
</body>
</html>
