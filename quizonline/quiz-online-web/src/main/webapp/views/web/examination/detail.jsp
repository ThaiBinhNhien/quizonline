<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Chi tiết đề thi</title>
</head>
<body>
<form action="<c:url value="/bai-thi-dap-an"/>" method="post" id="formUrl">
    <!-- START COUNTDOWN -->
    <script type="text/javascript">
        var time = ${time};
        function doneHandler(result) {
            var year = result.getFullYear();
            var month = result.getMonth() + 1; // bump by 1 for humans
            var day = result.getDate();
            var h = result.getHours();
            var m = result.getMinutes();
            var s = result.getSeconds();

            var UTC = result.toString();

            var output = UTC + "\n";
            output += "year: " + year + "\n";
            output += "month: " + month + "\n";
            output += "day: " + day + "\n";
            output += "h: " + h + "\n";
            output += "m: " + m + "\n";
            output += "s: " + s + "\n";
        }
        var myCountdownTest = new Countdown({
            time: ${time},
            width	: 300,
            height	: 50,
            onComplete : doneHandler
        });
    </script>
    <!-- END COUNTDOWN -->
<div class="image group">
    <div class="grid news_desc">
        <div style="overflow: auto; height: 500px" >
            <c:forEach items="${model.listResult}" var="item">
                <b>${item.number}.${item.question}</b>
                <p>
                        ${item.option1}
                </p>
                <p>
                        ${item.option2}
                </p>
                <p>
                        ${item.option3}
                </p>
                <p>
                        ${item.option4}
                </p>
                <p>
                    <input type="button" class="btn btn-primary" value="Gợi ý" data-toggle="modal" data-target="#myModal${item.id}"/>
                </p>
                <!-- The Modal -->
                <div class="modal fade" id="myModal${item.id}" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Gợi ý</h4>
                            </div>
                            <div class="modal-body">
                                <p>${item.suggest}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <div class="grid images_3_of_1">
        <div style="overflow: auto; height: 500px" >
            <c:forEach var="item" items="${model.listResult}">
                <c:if test="${not empty item.correctAnswer}">
                    ${item.number}.&nbsp;&nbsp;&nbsp;
                    A <input type="checkbox" name="answerUser[${item.id}]" value="A">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    B <input type="checkbox" name="answerUser[${item.id}]" value="B">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    C <input type="checkbox" name="answerUser[${item.id}]" value="C">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    D <input type="checkbox" name="answerUser[${item.id}]" value="D">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br/>
                    <br/>
                </c:if>
            </c:forEach>
        </div>
        <input type="hidden" name="examinationId" value="${examinationId}"/>
        <input type="submit" class="btn btn-primary" value="Nộp bài"/>
    </div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        timeOut(${time}000);
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    });
    function timeOut(time) {
        setTimeout(function(){
            $('#formUrl').submit();
        }, time);
    }
</script>
</body>
</html>
