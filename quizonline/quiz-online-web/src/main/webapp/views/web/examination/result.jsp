<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Kết quả thi</title>
</head>
<body>
    <div class="image group">
        <div class="grid news_desc">
            <div style="overflow: auto; height: 500px" >
                <c:forEach items="${model.listResult}" var="item">
                    <b>${item.number}.${item.question}</b>
                    <p>
                        <c:if test="${fn:contains(item.correctAnswer,'A')}">
                            <img src="<c:url value="/template/image/correct.png"/>">
                        </c:if>                        	
                            ${item.option1}
                    </p>
                    <p>
                        <c:if test="${fn:contains(item.correctAnswer,'B')}">
                            <img src="<c:url value="/template/image/correct.png"/>">
                        </c:if>                        	
                            ${item.option2}
                    </p>
                    <p>
                        <c:if test="${fn:contains(item.correctAnswer,'C')}">
                            <img src="<c:url value="/template/image/correct.png"/>">
                        </c:if>                      	
                            ${item.option3}
                    </p>
                    <p>
                        <c:if test="${fn:contains(item.correctAnswer,'D')}">
                            <img src="<c:url value="/template/image/correct.png"/>">
                        </c:if>                      	
                            ${item.option4}
                    </p>
                </c:forEach>
            </div>
            <br/>
            <a href="/danh-sach-bai-thi" class="btn btn-primary" type="button">Trở lại danh sách</a>
        </div>
        <div class="grid images_3_of_1">
            <div style="overflow: auto; height: 500px" >
                <c:forEach var="item" items="${model.listResult}">
                    <c:if test="${not empty item.correctAnswer}">
                        ${item.number}.&nbsp;&nbsp;&nbsp;
                        A <input type="checkbox" value="A" ${fn:contains(item.answerUser,'A') ? 'checked':''} disabled>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        B <input type="checkbox" value="B" ${fn:contains(item.answerUser,'B') ? 'checked':''} disabled>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        C <input type="checkbox" value="C" ${fn:contains(item.answerUser,'C') ? 'checked':''} disabled>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        D <input type="checkbox" value="D" ${fn:contains(item.answerUser,'D') ? 'checked':''} disabled>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <br/>
                        <br/>
                    </c:if>
                </c:forEach>
            </div>
            <br/>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Xem kết quả</button>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Xem kết quả thi</h4>
                </div>
                <div class="modal-body">
                    <p>Số câu đúng / Tổng số câu: ${correctNumber}/${totalNumber}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {

    });
</script>
</body>
</html>
