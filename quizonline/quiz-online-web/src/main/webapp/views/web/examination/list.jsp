<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<c:url var="urlList" value="/danh-sach-bai-thi"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Danh sách bài thi</title>
</head>
<body>
<div class="main">
    <form action="${urlList}" method="get" id="formUrl">
        <div class="content">
            <div class="col span_2_of_3">
                <div class="contact-form">
                    <div>
                                <span>
                                    <input name="name" type="text" class="textbox" value="${model.name}"/>
                                </span>
                    </div>
                </div>
            </div>
            <c:if test="${not empty model.listResult}">
                <c:forEach var="items" items="${model.listResult}">
                    <div class="image group">
                        <div class="grid news_desc">
                            <h3>${items.name}</h3>
                            <c:if test="${not empty USERNAME}">
                                <c:url value="/bai-thi-thuc-hanh" var="detailUrl">
                                    <c:param name="id" value="${items.id}"/>
                                </c:url>
                                <c:if test="${items.count == items.maxIteration}">
                                    <h4><span>Bạn đã thi đủ số lần quy định của bài thi</span></h4>
                                </c:if>
                                <c:if test="${items.count < items.maxIteration}">
                                    <h4><span><a href="${detailUrl}">Làm bài thi</a></span></h4>
                                </c:if>
                            </c:if>
                            <c:if test="${empty USERNAME}">
                                <h4><span>Yêu cầu đăng nhập trước khi làm bài</span></h4>
                            </c:if>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
            <c:if test="${empty model.listResult}">
                <div class="image group">
                    <div class="grid news_desc">
                        <h3>Không có bài thi nào</h3>
                </div>
            </c:if>
            <ul id="pagination-demo" class="pagination-sm"></ul>
            <form:hidden path="page" id="page"/>
        </div>
    </form>
</div>
<script type="text/javascript">
    var totalPages = ${model.totalPages};
    var startPage = ${model.page};
    var visiblePages = ${model.maxPageItems};
    $(document).ready(function () {
        $('#btnSearch').click(function () {
            $('#formUrl').submit();
        });
        
        $( "#home" ).removeClass( "active" );
        $( "#listexample" ).addClass( "active" );
    });
    $(function () {
        var obj = $('#pagination-demo').twbsPagination({
            totalPages: totalPages,
            startPage: startPage,
            visiblePages: visiblePages,
            onPageClick: function (event, page) {
                if (page != startPage) {
                    $('#page').val(page);
                    $('#formUrl').submit();
                }
            }
        });
    });
</script>
</body>
</html>