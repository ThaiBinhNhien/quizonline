<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<%@ include file="/common/taglib.jsp"%>
<c:url var="formUrl" value="/dang-nhap" />
<html>
<head>
<title>Đăng nhập</title>
</head>
<body>
	<div class="main">
		<div class="content">
			<div class="section group">
				<div class="col span_2_of_3">
					<div class="contact-form">
						<h2>Đăng nhập</h2>
						<form method="post" action="${formUrl}">
							<p style="color:red;">${param.message}</p>
							<div>
								<span><label>Tên đăng nhập</label></span> <span><input
									name="name" type="text" class="textbox"></span>
							</div>
							<div>
								<span><label>Mật khẩu</label></span> <span><input
									name="password" type="password" class="textbox"></span>
							</div>
							<div>
								<span><input type="submit" value="Đăng nhập"
									class="myButton"></span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
