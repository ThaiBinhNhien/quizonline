<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:url var="updateUserUrl" value="/cap-nhat-tai-khoan"/>
<c:url var="registerUrl" value="/dang-ky"/>
<html>
<head>
    <title>Quản lý tài khoản</title>
</head>
<body>
<div class="main">
    <div class="content">
        <div class="section group">
            <div class="col span_2_of_3">
                <div class="contact-form">
                    <h2>Đăng ký tài khoản</h2>
                        <c:if test="${not empty messageResponse}">
                            <div>
                                <span><label style="color:red">${messageResponse}</label></span>
                            </div>
                        </c:if>
                    <form method="post" action="" id="formSubmit">
                        <div>
                            <span><label>Tên đăng nhập</label></span>
                            <span><input name="name" type="text" class="textbox" value="${model.name}"></span>
                        </div>
                        <div>
                            <span><label>Tên đầy đủ</label></span>
                            <span><input name="fullName" type="text" class="textbox" value="${model.fullName}"></span>
                        </div>
                        <div>
                            <span><label>Mật khẩu</label></span>
                            <span><input name="password" type="password" class="textbox" value="${model.password}"></span>
                        </div>
                        <div>
                            <c:if test="${empty model.id}">
                                <span><input type="button" value="Đăng ký"  class="myButton" id="btnRegister"></span>
                            </c:if>
                            <c:if test="${not empty model.id}">
                                <input type="hidden" value="${model.id}" name="id"/>
                                <span><input type="button" value="Cập nhật tài khoản"  class="myButton" id="btnUpdate"></span>
                            </c:if>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

    });
    $('#btnRegister').click(function (e) {
        e.preventDefault();
        $('#formSubmit').attr('action', '${registerUrl}');
        $('#formSubmit').submit();
    });
    $('#btnUpdate').click(function (e) {
        e.preventDefault();
        $('#formSubmit').attr('action', '${updateUserUrl}');
        $('#formSubmit').submit();
    })
</script>
</body>
</html>
