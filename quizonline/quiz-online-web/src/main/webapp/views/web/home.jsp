<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Trang chủ</title>
<style type="text/css">

.mySlides {
	display: none;
}

img {
	vertical-align: middle;
}

/* Slideshow container */
.slideshow_container {
	max-width: 1000px;
	position: relative;
	margin: auto;
}

/* Caption text */
.text {
	color: black;
	font-size: 15px;
	padding: 8px 12px;
	position: absolute;
	bottom: 8px;
	width: 100%;
	text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
	color: black;
	font-size: 12px;
	padding: 8px 12px;
	position: absolute;
	top: 0;
}

/* The dots/bullets/indicators */
.dot {
	height: 15px;
	width: 15px;
	margin: 0 2px;
	background-color: #bbb;
	border-radius: 50%;
	display: inline-block;
	transition: background-color 0.6s ease;
}

.active {
	background-color: #717171;
}

/* Fading animation */
.fade {
	-webkit-animation-name: fade;
	-webkit-animation-duration: 1.5s;
	animation-name: fade;
	animation-duration: 1.5s;
}

@
-webkit-keyframes fade {
	from {opacity: .4
}

to {
	opacity: 1
}

}
@
keyframes fade {
	from {opacity: .4
}

to {
	opacity: 1
}

}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
	.text {
		font-size: 11px
	}
}
</style>
</head>
<body>
	<div class="header_slide">
		<div class="header_bottom_left">
			<div class="categories">
				<ul>
					<h3>Mục lục</h3>
					<c:forEach var="item" items="${category}">
						<li><a
							href="<c:url value="/danh-sach-lop-hoc?categorycode=${item.code}"/>">${item.name}</a>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="header_bottom_right">
			<div class="slideshow_container" >
				<div class="mySlides">
					<img
						src="${pageContext.request.contextPath}/template/image/banner.jpg"
						style="width: 100%">
				</div>
				<div class="mySlides">
					<img
						src="${pageContext.request.contextPath}/template/image/banner1.jpg"
						style="width: 100%">
				</div>
				<div class="mySlides">
					<img
						src="${pageContext.request.contextPath}/template/image/banner2.jpg"
						style="width: 100%">
				</div>
			</div>
			<div style="text-align: center">
				<span class="dot"></span> 
				<span class="dot"></span> 
				<span class="dot"></span>
			</div>
		</div>
		<!--end bottom right-->
		<div class="clear"></div>
	</div>
	<div class="main"></div>
	<script>
		var slideIndex = 0;
		showSlides();

		function showSlides() {
			var i;
			var slides = document.getElementsByClassName("mySlides");
			var dots = document.getElementsByClassName("dot");
			for (i = 0; i < slides.length; i++) {
				slides[i].style.display = "none";
			}
			slideIndex++;
			if (slideIndex > slides.length) {
				slideIndex = 1
			}
			for (i = 0; i < dots.length; i++) {
				dots[i].className = dots[i].className.replace(" active", "");
			}
			slides[slideIndex - 1].style.display = "block";
			dots[slideIndex - 1].className += " active";
			setTimeout(showSlides, 4000); // Change image every 2 seconds
		}
	</script>

</body>
</html>