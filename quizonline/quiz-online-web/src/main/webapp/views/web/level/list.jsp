<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:url var="urlList" value="/danh-sach-lop-hoc"/>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<form action="${urlList}" method="get" id="formUrl">
    <div class="main">
        <div class="content">
            <c:forEach var="item" items="${model.listResult}">
                <div class="image group">
                    <div class="grid images_3_of_1">
                        <img src="${pageContext.request.contextPath}/template/image/default.jpg" alt="" />
                    </div>
                    <div class="grid news_desc">
                        <h3>${item.name}</h3>
                        <c:url value="/danh-sach-bai-thi" var="detailUrl">
                            <c:param name="categoryCode" value="${categorycode}"/>
                            <c:param name="levelCode" value="${item.code}"/>
                        </c:url>
                        <h4><span><a href="${detailUrl}">Danh sách thi theo thể loại và level</a></span></h4>
                    </div>
                </div>
            </c:forEach>
            <ul id="pagination-demo" class="pagination-sm"></ul>
        </div>
    </div>
    <input type="hidden" id="page" name="page"/>
</form>
<script type="text/javascript">
    var totalPages = ${model.totalPages};
    var startPage = ${model.page};
    var visiblePages = ${model.maxPageItems};
    $(document).ready(function () {
    });
    $(function () {
        var obj = $('#pagination-demo').twbsPagination({
            totalPages: totalPages,
            startPage: startPage,
            visiblePages: visiblePages,
            onPageClick: function (event, page) {
                if (page != startPage) {
                    $('#page').val(page);
                    $('#formUrl').submit();
                }
            }
        });
    });
</script>
</body>
</html>