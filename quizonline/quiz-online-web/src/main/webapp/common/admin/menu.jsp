<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="sidebar"
	class="sidebar                  responsive                    ace-save-state">
	<script type="text/javascript">
        try{ace.settings.loadState('sidebar')}catch(e){}
    </script>
	<div class="sidebar-shortcuts">
		<div class="sidebar-shortcuts-large">
			<button class="btn btn-success">
				<i class="ace-icon fa fa-signal"></i>
			</button>

			<button class="btn btn-info">
				<i class="ace-icon fa fa-pencil"></i>
			</button>

			<button class="btn btn-warning">
				<i class="ace-icon fa fa-users"></i>
			</button>

			<button class="btn btn-danger">
				<i class="ace-icon fa fa-cogs"></i>
			</button>
		</div>
		<div class="sidebar-shortcuts-mini">
			<span class="btn btn-success"></span> <span class="btn btn-info"></span>

			<span class="btn btn-warning"></span> <span class="btn btn-danger"></span>
		</div>
	</div>
	<ul class="nav nav-list">
		<li><a href="#" class="dropdown-toggle"> <i
				class="menu-icon fa fa-list"></i> <span class="menu-text"></span> Dữ
				liệu nền <b class="arrow fa fa-angle-down"></b>
		</a> <b class="arrow"></b>
			<ul class="submenu nav-show" style="display: block;">
				<c:if test="${ROLE == 'ADMIN'}">
					<li><c:url value="/admin-categories-list" var="subjectListUrl">
							<c:param name="type" value="list" />
						</c:url> <a href="${subjectListUrl}"> <i
							class="menu-icon fa fa-caret-right"></i> Quản lý môn thi
					</a> <b class="arrow"></b></li>
					<li><c:url value="/admin-level-list" var="classListUrl">
							<c:param name="type" value="list" />
						</c:url> <a href="${classListUrl}"> <i
							class="menu-icon fa fa-caret-right"></i> Quản lý lớp thi
					</a> <b class="arrow"></b></li>
					<li><c:url value="/admin-user-list" var="userListUrl">
							<c:param name="type" value="list" />
						</c:url> <a href="${userListUrl}"> <i
							class="menu-icon fa fa-caret-right"></i> Quản lý người dùng
					</a> <b class="arrow"></b></li>
					<li><c:url var="userResults" value="/admin-user-result" /> <a
						href="${userResults}"> <i class="menu-icon fa fa-caret-right"></i>
							Quản lý kết quả thi của user
					</a> <b class="arrow"></b></li>
				</c:if>
				<li><c:url var="listExaminationUrl"
						value="/admin-examination-list">
						<c:param name="type" value="list" />
					</c:url> <a href="${listExaminationUrl}"> <i
						class="menu-icon fa fa-caret-right"></i> Quản lý đề thi
				</a> <b class="arrow"></b></li>
			</ul></li>
	</ul>
	<div class="sidebar-toggle sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left ace-save-state"
			data-icon1="ace-icon fa fa-angle-double-left"
			data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>