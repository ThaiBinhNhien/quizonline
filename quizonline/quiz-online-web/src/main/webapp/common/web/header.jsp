<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="header">
    <div class="headertop_desc">
        <div class="call">
            <p>
                <span>Need help?</span> call us
                <span class="number">1-22-3456789</span>

            </p>
        </div>
        <div class="account_desc">
            <ul>
                <c:if test="${empty USERNAME}">
                    <li>
                        <a href='<c:url value="/dang-ky"/>'>Đăng ký</a>
                    </li>
                    <li>
                        <a href='<c:url value="/dang-nhap?action=login"/>'>Đăng nhập</a>
                    </li>
                </c:if>
                <c:if test="${not empty USERNAME}">
                    <li>
                        Welcome, <a href='<c:url value="/cap-nhat-tai-khoan"/>'>${USERNAME}</a>
                    </li>
                    <li>
                        <a href='<c:url value="/ket-qua-thi"/>'>Kết quả thi</a>
                    </li>
                    <li>
                        <a href='<c:url value="/thoat?action=logout"/>'>Thoát</a>
                    </li>
                </c:if>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div class="header_top">
        <div class="logo">
            <a href="#">
                <img src="<c:url value='/template/web/images/logo.png' />" alt="">
            </a>
        </div>
        <script type="text/javascript">
            function DropDown(el) {
                this.dd = el;
                this.initEvents();
            }
            DropDown.prototype = {
                initEvents: function () {
                    var obj = this;

                    obj.dd.on('click', function (event) {
                        $(this).toggleClass('active');
                        event.stopPropagation();
                    });
                }
            }

            $(function () {
                var dd = new DropDown($('#dd'));
                $(document).click(function () {
                    // all dropdowns
                    $('.wrapper-dropdown-2').removeClass('active');
                });
            });
        </script>
        <div class="clear">

        </div>
    </div>
    <div class="header_bottom">
        <div class="menu">
            <ul>
                <li id="home" class="active">
                    <a href='<c:url value="/trang-chu"/>'>Trang chủ</a>
                </li>
                <li id="listexample">
                    <a href="<c:url value="/danh-sach-bai-thi"/>">Danh sách đề thi</a>
                </li>
                <li>
                    <a href="#">Gioi thiệu</a>
                </li>
                <li>
                    <a href="#">Liên hệ</a>
                </li>
                <div class="clear"></div>
            </ul>
        </div>
        <div class="clear"></div>
    </div>

</div>