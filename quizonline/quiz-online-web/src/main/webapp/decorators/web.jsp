<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><dec:title default="Trang chủ" /></title>

    <!-- css -->
    <link href="<c:url value='/template/web/css/style.css' />" rel="stylesheet" type="text/css" media="all"/>
    <link href="<c:url value='/template/web/css/slider.css' />" rel="stylesheet" type="text/css" media="all"/>

    <!-- jquery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<c:url value="/template/web/js/paging/jquery.twbsPagination.js"/>" type="text/javascript"></script>

    <script src="<c:url value="/template/countdown/countdown.js"/>" type="text/javascript"></script>

    <script src="<c:url value="/template/jquery.popupoverlay.js"/>" type="text/javascript"></script>

</head>
<body>
<div class="wrap">
    <!-- header -->
    <%@ include file="/common/web/header.jsp" %>
    <!-- header -->
    <dec:body/>
</div>

<!-- footer -->
<%@ include file="/common/web/footer.jsp" %>
<!-- footer -->

<script type="text/javascript">
    $(document).ready(function () {
        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<a href="#" id="toTop">
    <span id="toTopHover"> </span>
</a>

<!-- javascript -->
<script type="text/javascript" src="<c:url value='/template/web/js/move-top.js' />"></script>
<script type="text/javascript" src="<c:url value='/template/web/js/easing.js' />"></script>
<script type="text/javascript" src="<c:url value='/template/web/js/startstop-slider.js' />"></script>

</body>
</html>