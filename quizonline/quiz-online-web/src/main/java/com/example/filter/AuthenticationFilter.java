package com.example.filter;

import com.example.utils.SessionUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

	private ServletContext context;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.context = filterConfig.getServletContext();
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) servletRequest;
		HttpServletResponse res = (HttpServletResponse) servletResponse;
		String url = req.getRequestURI();
		String role = (String) SessionUtil.getInstance().getValue(req, "ROLE");
		if (url.contains("admin")) {
			if (role != null && (role.equals("ADMIN") || role.equals("MANAGER"))) {
				filterChain.doFilter(servletRequest, servletResponse);
			} else {
				res.sendRedirect(req.getContextPath()+"/trang-chu");
			}
		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}
 	}

	@Override
	public void destroy() {
	}
}
