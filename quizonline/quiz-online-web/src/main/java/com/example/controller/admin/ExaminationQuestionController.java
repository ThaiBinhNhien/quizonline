package com.example.controller.admin;

import com.example.constant.SystemConstant;
import com.example.dto.ExaminationQuestionDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import com.example.service.IExaminationQuestionService;
import com.example.utils.ExcelPoiUtil;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;
import com.example.utils.UploadUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(urlPatterns = {"/admin-examination-question-list", "/admin-examination-question-import"})
public class ExaminationQuestionController extends HttpServlet {

	@Inject
	private IExaminationQuestionService examinationQuestionService;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ExaminationQuestionDTO model = FormUtil.populate(ExaminationQuestionDTO.class, request);
		String examinationIdStr = request.getParameter("id");
		model.setExaminationId(Long.parseLong(examinationIdStr));
		request.setAttribute("examinationId", model.getExaminationId());
		//Import question
		if (request.getParameter("type") != null && request.getParameter("type").equals("import-question-page")) {
			RequestDispatcher rd = request.getRequestDispatcher("/views/admin/examination/examinationquestion/import.jsp");
			rd.forward(request, response);
		} 
		//Delete all question
		else if (request.getParameter("type") != null && request.getParameter("type").equals("deleteall")) {
			Map<String, Object> properties = new HashMap<>();
			properties.put("examinationId", model.getExaminationId());
			properties.put("name", model.getSearchValue());
			RequestUtil.initSearchBean(request, model);
			Pagination a = model.getPagination();
			Sorting b = model.getSorting();
			List<ExaminationQuestionDTO> lstDelete = examinationQuestionService.findAll(properties, model.getPagination(), model.getSorting());
			List<Long> lstId = new ArrayList<Long>();
			if(lstDelete.size()>0) {
				for(ExaminationQuestionDTO item: lstDelete) {
					lstId.add(item.getId());
				}
				examinationQuestionService.deleteAll(lstId);
				response.sendRedirect(request.getContextPath()+"/admin-examination-question-list?id="+examinationIdStr+"&result=1");
			}
			else {
				response.sendRedirect(request.getContextPath()+"/admin-examination-question-list?id="+examinationIdStr+"&result=2");
			}
		}
		//Export 
		else {
			if (request.getParameter("time") != null) {
				String time = request.getParameter("time");
				request.setAttribute("link_download_export_examination", "exportexamination/"+time+".docx");
			}
			getExaminationQuestions(request, model);
			request.setAttribute(SystemConstant.MODEL, model);
			RequestDispatcher rd = request.getRequestDispatcher("/views/admin/examination/examinationquestion/list.jsp");
			rd.forward(request, response);
		}
	}

	private void getExaminationQuestions(HttpServletRequest request, ExaminationQuestionDTO model) {
		Map<String, Object> properties = new HashMap<>();
		properties.put("examinationId", model.getExaminationId());
		properties.put("name", model.getSearchValue());
		RequestUtil.initSearchBean(request, model);
		model.setListResult(examinationQuestionService.findAll(properties, model.getPagination(), model.getSorting()));
		model.setTotalItems(examinationQuestionService.getTotalItem(properties));
	}

	private Map<String,Object> buildSearchProperties(ExaminationQuestionDTO model) {
		Map<String, Object> properties = new HashMap<>();
		return properties;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UploadUtil uploadUtil = new UploadUtil();
		Set<String> value = new HashSet<String>();
		value.add("examinationId");
		Object[] objects = uploadUtil.writeOrUpdateFile(request, value, "excel");
		Long examinationId = null;
		if (objects != null) {
			String examinationIdStr = null;
			Map<String, String> mapValue = (Map<String, String>) objects[3];
			for (Map.Entry<String, String> item: mapValue.entrySet()) {
				if (item.getKey().equals("examinationId")) {
					examinationIdStr = item.getValue();
				}
			}
			String fileLocation = objects[1].toString();
			String fileName = objects[2].toString();
			examinationId = Long.parseLong(examinationIdStr);
			List<ExaminationQuestionDTO> examinationQuestionDTOS = returnValueFromExcel(fileName, fileLocation);
			examinationQuestionService.saveImport(examinationQuestionDTOS, examinationId);
		}
		response.sendRedirect(request.getContextPath()+"/admin-examination-question-list?id="+examinationId+"");
	}

	private List<ExaminationQuestionDTO> returnValueFromExcel(String fileName, String fileLocation) throws IOException {
		Workbook workbook = ExcelPoiUtil.getWorkBook(fileName, fileLocation);
		Sheet sheet = workbook.getSheetAt(0);
		List<ExaminationQuestionDTO> excelValues = new ArrayList<ExaminationQuestionDTO>();
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			Row row = sheet.getRow(i);
			ExaminationQuestionDTO userImportDTO = readDataFromExcel(row);
			excelValues.add(userImportDTO);
		}
		return excelValues;
	}

	private ExaminationQuestionDTO readDataFromExcel(Row row) {
		ExaminationQuestionDTO examinationQuestionImportDTO = new ExaminationQuestionDTO();
		examinationQuestionImportDTO.setQuestion(ExcelPoiUtil.getCellValue(row.getCell(0)));
		examinationQuestionImportDTO.setOption1(ExcelPoiUtil.getCellValue(row.getCell(1)));
		examinationQuestionImportDTO.setOption2(ExcelPoiUtil.getCellValue(row.getCell(2)));
		examinationQuestionImportDTO.setOption3(ExcelPoiUtil.getCellValue(row.getCell(3)));
		examinationQuestionImportDTO.setOption4(ExcelPoiUtil.getCellValue(row.getCell(4)));
		examinationQuestionImportDTO.setCorrectAnswer(ExcelPoiUtil.getCellValue(row.getCell(5)));
		examinationQuestionImportDTO.setSuggest(ExcelPoiUtil.getCellValue(row.getCell(6)));
		return examinationQuestionImportDTO;
	}
}
