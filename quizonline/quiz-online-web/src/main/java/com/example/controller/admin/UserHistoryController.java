package com.example.controller.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.service.IUserService;
import com.example.utils.RequestUtil;
import org.apache.commons.lang.StringUtils;

import com.example.constant.SystemConstant;
import com.example.dto.ResultDTO;
import com.example.service.IExaminationService;
import com.example.service.IResultService;
import com.example.utils.FormUtil;

@WebServlet(urlPatterns = {"/admin-user-result"})
public class UserHistoryController extends HttpServlet {
	
	private static final long serialVersionUID = -1185473563273748792L;

	@Inject
	private IResultService resultService;

	@Inject
	private IExaminationService examinationService;

	@Inject
	private IUserService userService;

	//Hiển thị danh sách kết quả thi của user
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResultDTO model = FormUtil.populate(ResultDTO.class, request);
		if (request.getParameter("time") != null) {
			String time = request.getParameter("time");
			request.setAttribute("link_download_export_user_result", "exportuserresult/"+time+".xlsx");
		}
		//Lấy danh sách bài thi
		model.setExaminations(examinationService.findAll());
		//Lấy danh sách user
		model.setUsers(userService.findAll());
		RequestUtil.initSearchBean(request, model);
		//Lấy danh sách kết qu
		model.setListResult(resultService.findAll(new HashMap<>(), model.getPagination(), model.getSorting()));
		//Lấy tổng số item kết quả
		model.setTotalItems(resultService.getTotalItem(new HashMap<>()));
		//Model cho view
		request.setAttribute(SystemConstant.MODEL, model);
		RequestDispatcher rd = request.getRequestDispatcher("/views/admin/result/list.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
