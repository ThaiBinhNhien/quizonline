package com.example.controller.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.constant.SystemConstant;
import com.example.dto.CategoryDTO;
import com.example.service.ICategoryService;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;

@WebServlet(urlPatterns = {"/danh-sach-mon-hoc"})
public class CategoryController extends HttpServlet {

	@Inject
	private ICategoryService categoryService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		CategoryDTO model = FormUtil.populate(CategoryDTO.class, request);
		getAllCategory(model);
		request.setAttribute("levelCode", request.getParameter("levelcode"));
		request.setAttribute(SystemConstant.MODEL, model);
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/category/list.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	private void getAllCategory(CategoryDTO model) {
		Map<String, Object> properties = buildMapProperties();
		RequestUtil.initSearchBeanManual(model);
		List<CategoryDTO> categories = categoryService.findAll(properties, model.getPagination(), model.getSorting());
		model.setListResult(categories);
		model.setTotalItems(categoryService.getTotalItem(properties));
		model.setTotalPages((int) Math.ceil((double) model.getTotalItems() / model.getMaxPageItems()));
	}

	private Map<String,Object> buildMapProperties() {
		Map<String, Object> properties = new HashMap<String, Object>();
		return properties;
	}
}
