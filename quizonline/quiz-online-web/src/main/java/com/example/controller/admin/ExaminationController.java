package com.example.controller.admin;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.example.constant.SystemConstant;
import com.example.dto.ExaminationDTO;
import com.example.entity.CategoryEntity;
import com.example.entity.LevelEntity;
import com.example.repository.ICategoryRepository;
import com.example.repository.ILevelRepository;
import com.example.service.IExaminationService;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;
import com.example.utils.SessionUtil;

@WebServlet(urlPatterns = {"/admin-examination-list", "/admin-examination-edit"})
public class ExaminationController extends HttpServlet {

	@Inject
	private IExaminationService examinationService;

	@Inject
	private ICategoryRepository categoryRepository;

	@Inject
	private ILevelRepository levelRepository;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ExaminationDTO model = FormUtil.populate(ExaminationDTO.class, request);
		request.setAttribute("categories", categoryRepository.findAll());
		request.setAttribute("levels", levelRepository.findAll());
		if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_LIST)) {
			Map<String, Object> mapProperty = buildProperty(request, model);
			RequestUtil.initSearchBean(request, model);
			model.setListResult(examinationService.findAll(mapProperty, model.getPagination(), model.getSorting(), null));
			model.setTotalItems(examinationService.getTotalItem(mapProperty));
			request.setAttribute("categoryCode", request.getParameter("categoryCode"));
			request.setAttribute("levelCode", request.getParameter("levelCode"));
			request.setAttribute(SystemConstant.MODEL, model);
			RequestDispatcher rd = request.getRequestDispatcher("/views/admin/examination/list.jsp");
			rd.forward(request, response);
		} else if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_EDIT)) {
			if (model.getId() != null) {
				model = examinationService.findById(model.getId());
			}
			request.setAttribute(SystemConstant.MODEL, model);
			RequestDispatcher rd = request.getRequestDispatcher("/views/admin/examination/edit.jsp");
			rd.forward(request, response);
		}
		else if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_DELETE)) {
			if (model.getId() != null) {
				examinationService.deletById(model.getId());
				response.sendRedirect("admin-examination-edit?type=list&result=1");
			}
			else {
				response.sendRedirect("admin-examination-edit?type=list&result=2");
			}
			
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("utf-8");
		ExaminationDTO model = FormUtil.populate(ExaminationDTO.class, request);
		String result = "";
		if (model.getId() != null) {
			examinationService.updateExamination(model);
			result = "2";
		} else {
			String name = (String) SessionUtil.getInstance().getValue(request, "USERNAME");
			model = examinationService.insertExamination(model, name);
			result = "1";
		}
		response.sendRedirect("admin-examination-edit?id="+model.getId()+"&type=edit&result=" + URLEncoder.encode(result, "UTF-8"));
	}

	private Map<String,Object> buildProperty(HttpServletRequest request, ExaminationDTO model) {
		String role = (String) SessionUtil.getInstance().getValue(request, "ROLE");
		Map<String, Object> map = new HashMap<>();
		if (StringUtils.isNotBlank(model.getName())) {
			map.put("name", model.getName());
		}
		if (StringUtils.isNotBlank(request.getParameter("categoryCode"))) {
			CategoryEntity category = categoryRepository.findOneByProperty("code", request.getParameter("categoryCode"));
			map.put("category", category);
		}
		if (StringUtils.isNotBlank(request.getParameter("levelCode"))) {
			LevelEntity level = levelRepository.findOneByProperty("code", request.getParameter("levelCode"));
			map.put("level", level);
		}
		if (role.equals("MANAGER")) {
			String name = (String) SessionUtil.getInstance().getValue(request, "USERNAME");
			map.put("createdBy", name);
		}
		return map;
	}
}
