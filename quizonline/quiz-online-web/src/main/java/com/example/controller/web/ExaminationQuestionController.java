package com.example.controller.web;

import com.example.constant.SystemConstant;
import com.example.dto.ExaminationQuestionDTO;
import com.example.dto.ResultDTO;
import com.example.service.IExaminationQuestionService;
import com.example.service.IExaminationService;
import com.example.service.IResultService;
import com.example.utils.FormUtil;
import com.example.utils.SessionUtil;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = {"/bai-thi-thuc-hanh","/bai-thi-dap-an"})
public class ExaminationQuestionController extends HttpServlet {

	private static final long serialVersionUID = -2365386632098222713L;

	@Inject
	private IExaminationQuestionService examinationQuestionService;

	@Inject
	private IExaminationService examinationService;

	@Inject
	private IResultService resultService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ExaminationQuestionDTO model = FormUtil.populate(ExaminationQuestionDTO.class, request);
		String examinationIdStr = request.getParameter("id");
		String userName = (String) SessionUtil.getInstance().getValue(request, "USERNAME");
		model.setExaminationId(Long.parseLong(examinationIdStr));
		getExaminationQuestion(model, userName);
		SessionUtil.getInstance().putValue(request, "EXAMINATIONQUESTIONS", model.getListResult());
		request.setAttribute("time", examinationService.findById(model.getExaminationId()).getTime() * 60);
		request.setAttribute("examinationId", model.getExaminationId());
		request.setAttribute(SystemConstant.MODEL, model);
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/examination/detail.jsp");
		rd.forward(request, response);
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ExaminationQuestionDTO model = new ExaminationQuestionDTO();
		Long examinationId = Long.parseLong(request.getParameter("examinationId"));
		model.setExaminationId(examinationId);
		model.setListResult((List<ExaminationQuestionDTO>) SessionUtil.getInstance().getValue(request, "EXAMINATIONQUESTIONS"));
		for (ExaminationQuestionDTO item: model.getListResult()) {
			String[] answerUsers = request.getParameterValues("answerUser["+item.getId()+"]");			
			if (answerUsers != null) {
				item.setAnswerUsers(answerUsers);
				item.setAnswerUser(Arrays.toString(answerUsers));
			}
			item.setCorrectAnswers(item.getCorrectAnswer().split(","));
		}
		String userName = (String) SessionUtil.getInstance().getValue(request, "USERNAME");
		ResultDTO resultDTO = resultService.saveResult(userName, examinationId, model.getListResult());
		request.setAttribute("examinationId", model.getExaminationId());
		request.setAttribute("correctNumber", resultDTO.getCorrectNumber());
		request.setAttribute("totalNumber", resultDTO.getTotalNumber());
		request.setAttribute(SystemConstant.MODEL, model);
		SessionUtil.getInstance().remove(request, "EXAMINATIONQUESTIONS");
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/examination/result.jsp");
		rd.forward(request, response);
	}

	private void getExaminationQuestion(ExaminationQuestionDTO model, String name) {
		Map<String, Object> properties = new HashMap<>();
		properties.put("examinationId", model.getExaminationId());
		model.setListResult(examinationQuestionService.findAll(properties, name));
	}
}
