package com.example.controller.web;

import com.example.constant.SystemConstant;
import com.example.dto.ResultDTO;
import com.example.service.IExaminationService;
import com.example.service.IResultService;
import com.example.utils.FormUtil;
import com.example.utils.SessionUtil;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/ket-qua-thi"})
public class UserResultController extends HttpServlet {

	private static final long serialVersionUID = -2052067823186538805L;

	@Inject
	private IResultService resultService;

	@Inject
	private IExaminationService examinationService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName = (String) SessionUtil.getInstance().getValue(request, "USERNAME");
		ResultDTO model = FormUtil.populate(ResultDTO.class, request);
		String code = request.getParameter("examinationCode");
		List<ResultDTO> results = resultService.getResultsByUser(userName, code);
		model.setExaminations(examinationService.findAll());
		if (code != null && StringUtils.isNotEmpty(code)) {
			request.setAttribute("selectedCode", code);
		}
		model.setListResult(results);
		request.setAttribute(SystemConstant.MODEL, model);
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/user/manage/detail.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
