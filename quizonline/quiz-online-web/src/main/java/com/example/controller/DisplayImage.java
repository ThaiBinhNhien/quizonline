package com.example.controller;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Admin on 13/11/2017.
 */
public class DisplayImage extends HttpServlet {

	private final String imagesBase = "/fileupload";

	//Hàm thực hiện chức năng download đề thi
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
		String imageUrl = request.getRequestURI();
		int strLenght = imageUrl.indexOf("/repository/") + "/repository/".length();
		String relativeImagePath = imageUrl.substring(strLenght);
		ServletOutputStream outStream;
		outStream = response.getOutputStream();
		String path = imagesBase + File.separator + relativeImagePath;
		FileInputStream fin = new FileInputStream(path);
		BufferedInputStream bin = new BufferedInputStream(fin);
		BufferedOutputStream bout = new BufferedOutputStream(outStream);
		int ch =0; ;
		while((ch=bin.read())!=-1)
			bout.write(ch);
		bin.close();
		fin.close();
		bout.close();
		outStream.close();
	}
}
