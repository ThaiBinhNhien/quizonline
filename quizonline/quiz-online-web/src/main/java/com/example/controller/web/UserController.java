package com.example.controller.web;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.example.constant.SystemConstant;
import com.example.dto.UserDTO;
import com.example.service.IUserService;
import com.example.utils.FormUtil;
import com.example.utils.SessionUtil;

@WebServlet(urlPatterns = {"/dang-ky", "/cap-nhat-tai-khoan"})
public class UserController extends HttpServlet {

	private static final long serialVersionUID = -1700389102708985293L;

	private final Logger log = Logger.getLogger(this.getClass());

	@Inject
	private IUserService userService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = (String) SessionUtil.getInstance().getValue(request, "USERNAME");
		UserDTO userDTO = new UserDTO();
		if (name != null) {
			userDTO = userService.findByName(name);
		}
		request.setAttribute(SystemConstant.MODEL, userDTO);
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/user/edit.jsp");
		rd.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDTO userDTO = FormUtil.populate(UserDTO.class, request);
		if (userDTO.getId() != null) {
			userService.updateUser(userDTO);
			request.setAttribute(SystemConstant.MESSAGE_RESPONSE, "Cập nhật thành công");
		} else {
			UserDTO userExist = userService.findByName(userDTO.getName());
			if (userExist != null) {
				request.setAttribute(SystemConstant.MESSAGE_RESPONSE, "Tên đăng nhập trùng");
			} else {
				userService.insertUser(userDTO);
				request.setAttribute(SystemConstant.MESSAGE_RESPONSE, "Đăng ký thành công");
			}			
		}
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/user/edit.jsp");
		rd.forward(request, response);
	}
}
