package com.example.controller.web;

import com.example.constant.SystemConstant;
import com.example.dto.ExaminationDTO;
import com.example.entity.CategoryEntity;
import com.example.entity.LevelEntity;
import com.example.repository.ICategoryRepository;
import com.example.repository.ILevelRepository;
import com.example.service.IExaminationService;
import com.example.service.IResultService;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;
import com.example.utils.SessionUtil;
import org.apache.commons.lang.StringUtils;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 3/12/2017.
 */
@WebServlet(urlPatterns = {"/danh-sach-bai-thi"})
public class ExaminationController extends HttpServlet {

	@Inject
	private IExaminationService examinationService;

	@Inject
	private IResultService resultService;

	@Inject
	private ICategoryRepository categoryRepository;

	@Inject
	private ILevelRepository levelRepository;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ExaminationDTO model = FormUtil.populate(ExaminationDTO.class, request);
		String userName = (String) SessionUtil.getInstance().getValue(request, "USERNAME");
		executeSearchExamination(request, model, userName);
		request.setAttribute(SystemConstant.MODEL, model);
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/examination/list.jsp");
		rd.forward(request, response);
	}

	private void executeSearchExamination(HttpServletRequest request, ExaminationDTO model, String userName) {
		Map<String, Object> properties = buildMapProperties(request, model);
		RequestUtil.initSearchBeanManual(model);
		model.setListResult(examinationService.findAll(properties, model.getPagination(), model.getSorting(), userName));
		model.setTotalItems(examinationService.getTotalItem(properties));
		model.setTotalPages((int) Math.ceil((double) model.getTotalItems() / model.getMaxPageItems()));
	}

	private Map<String,Object> buildMapProperties(HttpServletRequest request, ExaminationDTO model) {
		Map<String, Object> properties = new HashMap<String, Object>();
		if (StringUtils.isNotBlank(model.getName())) {
			properties.put("name", model.getName());
		}
		if (StringUtils.isNotBlank(request.getParameter("categoryCode"))) {
			CategoryEntity category = categoryRepository.findOneByProperty("code", request.getParameter("categoryCode"));
			properties.put("category", category);
		}
		if (StringUtils.isNotBlank(request.getParameter("levelCode"))) {
			LevelEntity level = levelRepository.findOneByProperty("code", request.getParameter("levelCode"));
			properties.put("level", level);
		}
		return properties;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
