package com.example.controller.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.constant.SystemConstant;
import com.example.dto.CategoryDTO;
import com.example.dto.LevelDTO;
import com.example.service.ICategoryService;
import com.example.service.ILevelService;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;

@WebServlet(urlPatterns = {"/danh-sach-lop-hoc"})
public class LevelController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	@Inject
	private ILevelService levelService;
	
	@Inject
	private ICategoryService iCategoryService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LevelDTO model = FormUtil.populate(LevelDTO.class, request);
		String code = request.getParameter("categorycode");
		CategoryDTO categoryDTO = iCategoryService.findByCategoryCode(code);
		getAllLevel(model, categoryDTO);
		request.setAttribute("categorycode", code);
		request.setAttribute(SystemConstant.MODEL, model);
		RequestDispatcher rd = request.getRequestDispatcher("/views/web/level/list.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
	private void getAllLevel(LevelDTO model, CategoryDTO categoryDTO) {
		List<LevelDTO> level = new ArrayList<LevelDTO>();
		String[] ids = categoryDTO.getLevels().split(",");
		for(int i = 0 ; i < ids.length ; i++) {
			Long id = Long.parseLong(ids[i]);
			Map<String, Object> properties = buildMapProperties(id);
			RequestUtil.initSearchBeanManual(model);
			level.addAll(levelService.findAll(properties, model.getPagination(), model.getSorting()));
		}
		model.setListResult(level);
		model.setTotalItems(level.size());
		model.setTotalPages((int) Math.ceil((double) model.getTotalItems() / model.getMaxPageItems()));
	}
	
	private Map<String,Object> buildMapProperties(Long idLevel) {
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("id", idLevel);
		return properties;
	}
}
