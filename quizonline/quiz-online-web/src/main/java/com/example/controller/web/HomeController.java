package com.example.controller.web;

import java.io.IOException;
import java.net.URLEncoder;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.example.dto.RoleDTO;
import com.example.dto.UserDTO;
import com.example.repository.ICategoryRepository;
import com.example.repository.ILevelRepository;
import com.example.service.IUserService;
import com.example.utils.FormUtil;
import com.example.utils.SessionUtil;

@WebServlet(urlPatterns = {"/trang-chu", "/dang-nhap", "/thoat"})
public class HomeController extends HttpServlet {

	private final Logger log = Logger.getLogger(this.getClass());

	@Inject
	private IUserService userService;

	@Inject
	private ICategoryRepository categoryRepository;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		if (action != null && action.equals("login")) {
			RequestDispatcher rd = request.getRequestDispatcher("/views/web/login.jsp");
			rd.forward(request, response);
		} else if(action != null && action.equals("logout")) {
			SessionUtil.getInstance().remove(request, "ROLE");
			SessionUtil.getInstance().remove(request, "USERNAME");
			response.sendRedirect(request.getContextPath()+"/trang-chu");
		} else {
			request.setAttribute("category", categoryRepository.findAll());
			RequestDispatcher rd = request.getRequestDispatcher("/views/web/home.jsp");
			rd.forward(request, response);
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDTO userDTO = FormUtil.populate(UserDTO.class, request);
		userDTO = userService.findByNameAndPassword(userDTO.getName(), userDTO.getPassword());
		if(userDTO!=null) {
			for (RoleDTO item: userDTO.getRoles()) {
				SessionUtil.getInstance().putValue(request, "ROLE", item.getCode());
				SessionUtil.getInstance().putValue(request, "USERNAME", userDTO.getName());
				if (item.getCode().equals("ADMIN") || item.getCode().equals("MANAGER")) {
					response.sendRedirect(request.getContextPath()+"/admin-home");
				} else if (item.getCode().equals("USER")) {
					response.sendRedirect(request.getContextPath()+"/trang-chu");
				}
			}
		}
		else {
			String message ="Thông tin đăng nhập không chính xác. Vui lòng thử lại!!";
			response.sendRedirect(request.getContextPath()+"/dang-nhap?action=login&message=" + URLEncoder.encode(message, "UTF-8"));
		}
	}
}
