package com.example.controller.admin;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.constant.SystemConstant;
import com.example.utils.UploadUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.example.entity.ExaminationQuestionEntity;
import com.example.repository.IExaminationQuestionRepository;
import com.example.repository.IExaminationRepository;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

@WebServlet(urlPatterns = "/admin-export-examination-question")
public class ExportExaminationQuestionController extends HttpServlet {
	
	private static final long serialVersionUID = 5061379809019365891L;

	private static final String FILE_NAME = "/fileupload/exportexamination/";

	@Inject
	private IExaminationQuestionRepository examinationQuestionRepository;
	
	@Inject
	private IExaminationRepository examinationRepository;

	//Export danh sách câu hỏi của đề thi
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("examination", examinationRepository.findById(id));
		List<ExaminationQuestionEntity> examinationQuestions = examinationQuestionRepository.findByProperty(maps);

		// Write the output to a file
		Timestamp time = new Timestamp(System.currentTimeMillis());
		UploadUtil uploadUtil = new UploadUtil();
		String address = "/"+ SystemConstant.FOLDER_UPLOAD;
		uploadUtil.checkAndCreateFolder(address,"exportexamination");
		XWPFDocument document = new XWPFDocument();
		try (FileOutputStream out = new FileOutputStream(FILE_NAME+time.getTime()+".docx")) {
			int i = 1;
			for (ExaminationQuestionEntity item: examinationQuestions) {
				XWPFParagraph paragraph = document.createParagraph();
				XWPFRun xwpfRun = paragraph.createRun();
				xwpfRun.setText("Câu "+ i +". "+item.getQuestion());
				xwpfRun.addBreak();
				xwpfRun.setText(item.getOption1());
				xwpfRun.addBreak();
				xwpfRun.setText(item.getOption2());
				xwpfRun.addBreak();
				xwpfRun.setText(item.getOption3());
				xwpfRun.addBreak();
				xwpfRun.setText(item.getOption4());
				System.out.println(paragraph.getText());
				i++;
			}

			document.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath()+"/admin-examination-question-list?id="+id+"&time="+time.getTime());
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
