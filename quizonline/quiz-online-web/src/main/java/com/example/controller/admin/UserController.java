package com.example.controller.admin;

import com.example.constant.SystemConstant;
import com.example.dto.UserDTO;
import com.example.repository.IRoleRepository;
import com.example.service.IUserService;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = {"/admin-user-list", "/admin-user-edit"})
public class UserController extends HttpServlet {

	private final Logger log = Logger.getLogger(this.getClass());

	@Inject
	private IUserService userService;

	@Inject
	private IRoleRepository roleRepository;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDTO model = FormUtil.populate(UserDTO.class, request);
		
		//Hiển thị danh sách user nếu type là list
		if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_LIST)) {
			Map<String, Object> mapProperty = buildProperty();
			RequestUtil.initSearchBean(request, model);
			model.setListResult(userService.findAll(mapProperty, model.getPagination(), model.getSorting()));
			model.setTotalItems(userService.getTotalItem(mapProperty));
			request.setAttribute(SystemConstant.MODEL, model);
			RequestDispatcher rd = request.getRequestDispatcher("/views/admin/user/list.jsp");
			rd.forward(request, response);
		} 
		else 
			// Thực hiện chức năng chỉnh sửa user
			if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_EDIT)) {
				if (model.getId() != null) {
					model = userService.findById(model.getId());
				}
				request.setAttribute("roles", roleRepository.findAll());
				request.setAttribute(SystemConstant.MODEL, model);
				RequestDispatcher rd = request.getRequestDispatcher("/views/admin/user/edit.jsp");
				rd.forward(request, response);
			}
	}

	private Map<String,Object> buildProperty() {
		Map<String, Object> map = new HashMap<>();
		map.put("status", 1);
		return map;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDTO model = FormUtil.populate(UserDTO.class, request);
		if (model.getId() != null) {
			//Lưu thông tin user chỉnh sửa
			userService.updateUser(model);
		} else {
			//Thêm mới thông tin user nếu chưa có id
			model = userService.insertUser(model);
		}
		response.sendRedirect("/admin-user-edit?id="+model.getId()+"&type=edit");
	}
}
