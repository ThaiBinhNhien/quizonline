package com.example.controller.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.constant.SystemConstant;
import com.example.dto.CategoryDTO;
import com.example.dto.LevelDTO;
import com.example.entity.LevelEntity;
import com.example.repository.ILevelRepository;
import com.example.service.ICategoryService;
import com.example.service.ILevelService;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;

@WebServlet(urlPatterns = {"/admin-categories-list", "/admin-categories-edit"})
public class CategoriesController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private ICategoryService categoryService;
	@Inject
	private ILevelService iLevelService;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		CategoryDTO model = FormUtil.populate(CategoryDTO.class, request);
		
		//Hiển thị danh sách môn nếu type là list
		if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_LIST)) {
			Map<String, Object> mapProperty = buildProperty();
			RequestUtil.initSearchBeanManual(model);
			List<CategoryDTO> categories = categoryService.findAll(mapProperty, model.getPagination(), model.getSorting());
			model.setListResult(categories);
			model.setTotalItems(categoryService.getTotalItem(mapProperty));
			request.setAttribute(SystemConstant.MODEL, model);
			RequestDispatcher rd = request.getRequestDispatcher("/views/admin/category/list.jsp");
			rd.forward(request, response);
		} 
		else {
			// Thực hiện chức năng chỉnh sửa danh sach mon hoc
			if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_EDIT)) {
				if (model.getId() != null) {
					model = categoryService.findById(model.getId());
				}
				List<LevelDTO> lstLevel = iLevelService.findAll();
				request.setAttribute("levels", lstLevel);
				request.setAttribute(SystemConstant.MODEL, model);
				RequestDispatcher rd = request.getRequestDispatcher("/views/admin/category/edit.jsp");
				rd.forward(request, response);
			}
			else if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_DELETE)) {
				if (model.getId() != null) {
					categoryService.deletById(model.getId());
					response.sendRedirect("admin-categories-list?type=list&result=1");
				}
				else {
					response.sendRedirect("admin-categories-list?type=list&result=2");
				}
				
			}
		}
	}

	private Map<String,Object> buildProperty() {
		Map<String, Object> map = new HashMap<>();
		return map;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("utf-8");
		CategoryDTO model = FormUtil.populate(CategoryDTO.class, request);
		if (model.getId() != null) {
			//Lưu thông tin user chỉnh sửa
			categoryService.updateCategory(model);
		} else {
			//Thêm mới thông tin user nếu chưa có id
			model = categoryService.insertCategory(model);
		}
		response.sendRedirect("admin-categories-edit?id="+model.getId()+"&type=edit");
	}
}
