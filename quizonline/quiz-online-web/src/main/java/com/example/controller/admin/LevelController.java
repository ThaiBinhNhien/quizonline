package com.example.controller.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.constant.SystemConstant;
import com.example.dto.LevelDTO;
import com.example.service.ILevelService;
import com.example.utils.FormUtil;
import com.example.utils.RequestUtil;

@WebServlet(urlPatterns = {"/admin-level-list", "/admin-level-edit"})
public class LevelController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	@Inject
	private ILevelService iLevelService;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LevelDTO model = FormUtil.populate(LevelDTO.class, request);
		
		//Hiển thị danh sách môn nếu type là list
		if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_LIST)) {
			Map<String, Object> mapProperty = buildProperty();
			RequestUtil.initSearchBeanManual(model);
			List<LevelDTO> levels = iLevelService.findAll(mapProperty, model.getPagination(), model.getSorting());
			model.setListResult(levels);
			model.setTotalItems(iLevelService.getTotalItem(mapProperty));
			request.setAttribute(SystemConstant.MODEL, model);
			RequestDispatcher rd = request.getRequestDispatcher("/views/admin/level/list.jsp");
			rd.forward(request, response);
		} 
		else {
			// Thực hiện chức năng chỉnh sửa user
			if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_EDIT)) {
				if (model.getId() != null) {
					model = iLevelService.findById(model.getId());
				}
				request.setAttribute(SystemConstant.MODEL, model);
				RequestDispatcher rd = request.getRequestDispatcher("/views/admin/level/edit.jsp");
				rd.forward(request, response);
			}
			else if (model.getType() != null && model.getType().equals(SystemConstant.TYPE_DELETE)) {
				if (model.getId() != null) {
					iLevelService.deletById(model.getId());
					response.sendRedirect("admin-level-list?type=list&result=1");
				}
				else {
					response.sendRedirect("admin-level-list?type=list&result=2");
				}
				
			}
		}
	}

	private Map<String,Object> buildProperty() {
		Map<String, Object> map = new HashMap<>();
		return map;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("utf-8");
		LevelDTO model = FormUtil.populate(LevelDTO.class, request);
		if (model.getId() != null) {
			//Lưu thông tin user chỉnh sửa
			iLevelService.updateLevel(model);
		} else {
			//Thêm mới thông tin user nếu chưa có id
			model = iLevelService.insertLevel(model);
		}
		response.sendRedirect("admin-level-edit?id="+model.getId()+"&type=edit");
	}
	
}
