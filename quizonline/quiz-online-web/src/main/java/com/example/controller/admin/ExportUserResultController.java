package com.example.controller.admin;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.constant.SystemConstant;
import com.example.dto.ResultDTO;
import com.example.entity.UserEntity;
import com.example.repository.IUserRepository;
import com.example.utils.FormUtil;
import com.example.utils.UploadUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.example.entity.ExaminationEntity;
import com.example.entity.ResultEntity;
import com.example.repository.IExaminationRepository;
import com.example.repository.IResultRepository;

@WebServlet(urlPatterns = "/admin-export-user-result")
public class ExportUserResultController extends HttpServlet {

	private static final long serialVersionUID = -6944449540510756830L;

	private static final String FILE_NAME = "/fileupload/exportuserresult/";
	
	@Inject
	private IResultRepository resultRepository;
	
	@Inject
	private IExaminationRepository examinationRepository;

	@Inject
	private IUserRepository userRepository;

	//Export kết quả thi của user ra file excel
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResultDTO model = FormUtil.populate(ResultDTO.class, request);

		Timestamp fromDateTimestamp = null;
		Timestamp toDateTimestamp = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if (model.getFromDate() != null && StringUtils.isNotEmpty(model.getFromDate())) {
				Date date = formatter.parse(model.getFromDate());
				fromDateTimestamp = new Timestamp(date.getTime());
			}

			if (model.getToDate() != null && StringUtils.isNotEmpty(model.getToDate())) {
				Date date = formatter.parse(model.getToDate());
				toDateTimestamp = new Timestamp(date.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// lấy thông tin kết quả thi
		List<ResultEntity> resultEntities = new ArrayList<>();
		if (model.getNames() != null && model.getNames().length > 0) {
			for (String item: model.getNames()) {
				Map<String, Object> properties = getProperties(model.getExaminationCode(), item);
				List<ResultEntity> resultEntities1 = resultRepository.findByNameAndExaminationAndCreatedDate(properties, fromDateTimestamp, toDateTimestamp);
				resultEntities.addAll(resultEntities1);
			}
		} else {
			Map<String, Object> properties = getProperties(model.getExaminationCode(), null);
			resultEntities = resultRepository.findByNameAndExaminationAndCreatedDate(properties, fromDateTimestamp, toDateTimestamp);
		}

		//Tạo cột cho file excel
		String[] columns = {"Đề thi", "Số câu đúng/Tổng số câu", "Tên người làm", "Ngày làm bài thi"};

		Workbook workbook = new XSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();

		Sheet sheet = workbook.createSheet("User Result");

		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row headerRow = sheet.createRow(0);

		//Tạo style cho cột
		for(int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		//Đổ dữ liệu cho cột
		int rowNum = 1;
		for(ResultEntity item: resultEntities) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(item.getExamination().getName());
			row.createCell(1).setCellValue(item.getCorrectNumber()+"/"+item.getTotalNumber());
			row.createCell(2).setCellValue(item.getUser().getName());
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			row.createCell(3).setCellValue(dateFormat.format(item.getCreatedDate()));
		}

		for(int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		//Tạo file, lưu file
		Timestamp time = new Timestamp(System.currentTimeMillis());
		UploadUtil uploadUtil = new UploadUtil();
		String address = "/"+ SystemConstant.FOLDER_UPLOAD;
		uploadUtil.checkAndCreateFolder(address,"exportuserresult");
		FileOutputStream fileOut = new FileOutputStream(FILE_NAME+time.getTime()+".xlsx");
		workbook.write(fileOut);
		fileOut.close();

		workbook.close();

		response.sendRedirect(request.getContextPath()+"/admin-user-result?time="+time.getTime());
	}

	private Map<String,Object> getProperties(String examinationCode, String name) {
		Map<String, Object> properties = new HashMap<>();
		if (examinationCode != null && StringUtils.isNotEmpty(examinationCode)) {
			ExaminationEntity examinationEntity = examinationRepository.findOneByProperty("code", examinationCode);
			properties.put("examination", examinationEntity);
		}
		if (name != null && StringUtils.isNotEmpty(name)) {
			UserEntity userEntity = userRepository.findOneByProperty("name", name);
			properties.put("user", userEntity);
		}
		return properties;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
