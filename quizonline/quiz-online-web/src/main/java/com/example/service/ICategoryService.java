package com.example.service;

import java.util.List;
import java.util.Map;

import com.example.dto.CategoryDTO;
import com.example.dto.UserDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

public interface ICategoryService {
	List<CategoryDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting);
	int getTotalItem(Map<String, Object> searchProperty);
	CategoryDTO findById(Long id);
	CategoryDTO findByCategoryCode(String categoryCode);
	void updateCategory(CategoryDTO updateCategory);
	CategoryDTO insertCategory(CategoryDTO newCategory);
	void deletById(Long id);
}
