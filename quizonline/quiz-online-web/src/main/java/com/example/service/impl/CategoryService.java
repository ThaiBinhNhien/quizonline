package com.example.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.example.converter.CategoryConverter;
import com.example.dto.CategoryDTO;
import com.example.entity.CategoryEntity;
import com.example.repository.ICategoryRepository;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import com.example.service.ICategoryService;

public class CategoryService implements ICategoryService {

	@Inject
	private ICategoryRepository categoryRepository;

	@Inject
	private CategoryConverter categoryConverter;

	@Override
	public List<CategoryDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting) {
		List<CategoryDTO> result = new ArrayList<>();
		List<CategoryEntity> objects = categoryRepository.findAll(searchProperty, pagination, sorting);
		for (CategoryEntity item: objects) {
			result.add(categoryConverter.convertToDto(item));
		}
		return result;
	}

	@Override
	public int getTotalItem(Map<String, Object> searchProperty) {
		return Integer.parseInt(categoryRepository.getTotalItem(searchProperty).toString());
	}
	
	@Override
	public CategoryDTO findById(Long id) {
		CategoryEntity categoryEntity = categoryRepository.findById(id);
		CategoryDTO userDTO = categoryConverter.convertToDto(categoryEntity);
		return userDTO;
	}
	
	@Override
	public CategoryDTO findByCategoryCode(String categoryCode) {
		CategoryEntity categoryEntity = categoryRepository.findOneByProperty("code", categoryCode);
		CategoryDTO userDTO = categoryConverter.convertToDto(categoryEntity);
		return userDTO;
	}
	
	@Override
	public void updateCategory(CategoryDTO updateCategory) {
		CategoryEntity oldCategory = categoryRepository.findById(updateCategory.getId());
		oldCategory.setName(updateCategory.getName());
		oldCategory.setCode(updateCategory.getCode());
		oldCategory.setLevels(updateCategory.getLevels());
		oldCategory.setCreatedDate(updateCategory.getCreatedDate());
		oldCategory.setModifiedDate(new Timestamp(System.currentTimeMillis()));
		
		categoryRepository.update(oldCategory);
	}
	
	@Override
	public void deletById(Long id) {
		categoryRepository.delete(id);
	}

	@Override
	public CategoryDTO insertCategory(CategoryDTO newCategory) {
		CategoryEntity category = categoryConverter.convertToEntity(newCategory);
		category.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		category.setModifiedDate(new Timestamp(System.currentTimeMillis()));
		category.setName(newCategory.getName());
		category.setCode(newCategory.getCode());
		
		return categoryConverter.convertToDto(categoryRepository.save(category));
	}
	
}
