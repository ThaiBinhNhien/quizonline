package com.example.service;

import com.example.dto.ExaminationDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

import java.util.List;
import java.util.Map;

public interface IExaminationService {
	ExaminationDTO findById(Long id);
	void deletById(Long id);
	void updateExamination(ExaminationDTO updateExamination);
	ExaminationDTO insertExamination(ExaminationDTO newExamination, String name);
	List<ExaminationDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting, String name);
	int getTotalItem(Map<String, Object> searchProperty);
	List<ExaminationDTO> findAll();
}
