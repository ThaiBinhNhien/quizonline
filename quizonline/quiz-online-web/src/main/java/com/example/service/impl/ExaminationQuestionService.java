package com.example.service.impl;

import com.example.converter.ExaminationQuestionConverter;
import com.example.dto.ExaminationQuestionDTO;
import com.example.entity.ExaminationEntity;
import com.example.entity.ExaminationQuestionEntity;
import com.example.entity.UserEntity;
import com.example.repository.IExaminationQuestionRepository;
import com.example.repository.IExaminationRepository;
import com.example.repository.IResultRepository;
import com.example.repository.IUserRepository;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import com.example.service.IExaminationQuestionService;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExaminationQuestionService implements IExaminationQuestionService {

	@Inject
	private IExaminationRepository examinationRepository;

	@Inject
	private IExaminationQuestionRepository examinationQuestionRepository;

	@Inject
	private ExaminationQuestionConverter examinationQuestionConverter;

	@Inject
	private IUserRepository userRepository;

	@Inject
	private IResultRepository resultRepository;

	@Override
	public List<ExaminationQuestionDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting) {
		Long id = (Long) searchProperty.get("examinationId");
		String name = (String) searchProperty.get("name");
		searchProperty = new HashMap<>();
		searchProperty.put("examination", examinationRepository.findById(id));
		List<ExaminationQuestionEntity> examinationQuestionEntities = examinationQuestionRepository.findAllQuestion(searchProperty, pagination, sorting, name);
		List<ExaminationQuestionDTO> examinationQuestionDTOS = new ArrayList<>();
		for (ExaminationQuestionEntity item : examinationQuestionEntities) {
			ExaminationQuestionDTO examinationQuestionDTO = examinationQuestionConverter.convertToDto(item);
			examinationQuestionDTOS.add(examinationQuestionDTO);
		}
		return examinationQuestionDTOS;
	}

	@Override
	public void deleteAll(List<Long> ids) {
		examinationQuestionRepository.delete(ids);
	}
	@Override
	public List<ExaminationQuestionDTO> findAll(Map<String, Object> searchProperty, String name) {
		Long id = (Long) searchProperty.get("examinationId");
		searchProperty = new HashMap<>();
		searchProperty.put("examination", examinationRepository.findById(id));
		Long countFrequency = countUserDoExamination(id, name);
		List<ExaminationQuestionEntity> examinationQuestionEntities = new ArrayList<>();
		if (countFrequency >= 1) {
			String sql = " ORDER BY RAND() ";
			examinationQuestionEntities = examinationQuestionRepository.findAll(searchProperty, new Pagination(), new Sorting(), sql);
		} else {
			examinationQuestionEntities = examinationQuestionRepository.findAll(searchProperty, new Pagination(), new Sorting(), null);
		}
		List<ExaminationQuestionDTO> examinationQuestionDTOS = new ArrayList<>();
		int count = 1;
		for (ExaminationQuestionEntity item : examinationQuestionEntities) {
			ExaminationQuestionDTO examinationQuestionDTO = examinationQuestionConverter.convertToDto(item);
			examinationQuestionDTO.setNumber(count);
			count++;
			examinationQuestionDTOS.add(examinationQuestionDTO);
		}
		return examinationQuestionDTOS;
	}

	private Long countUserDoExamination(Long id, String name) {
		ExaminationEntity examination = examinationRepository.findById(id);
		UserEntity user = userRepository.findOneByProperty("name", name);
		Map<String, Object> maps = new HashMap<>();
		maps.put("user", user);
		maps.put("examination", examination);
		return resultRepository.countByProperty(maps);
	}

	@Override
	public int getTotalItem(Map<String, Object> searchProperty) {
		Long id = (Long) searchProperty.get("examinationId");
		String name = (String) searchProperty.get("name");
		searchProperty = new HashMap<>();
		searchProperty.put("examination", examinationRepository.findById(id));
		return Integer.parseInt(examinationQuestionRepository.getTotalItemQuestion(searchProperty, name).toString());
	}

	@Override
	public void saveImport(List<ExaminationQuestionDTO> examinationQuestionDTOS, Long examinationId) {
		for (ExaminationQuestionDTO dto : examinationQuestionDTOS) {
			ExaminationQuestionEntity entity = new ExaminationQuestionEntity();
			entity.setCorrectAnswer(dto.getCorrectAnswer());
			entity.setQuestion(dto.getQuestion());
			entity.setOption1(dto.getOption1());
			entity.setOption2(dto.getOption2());
			entity.setOption3(dto.getOption3());
			entity.setOption4(dto.getOption4());
			entity.setSuggest(dto.getSuggest());
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			entity.setCreatedDate(timestamp);
			ExaminationEntity examination = examinationRepository.findById(examinationId);
			entity.setExamination(examination);
			examinationQuestionRepository.save(entity);
		}
	}
}
