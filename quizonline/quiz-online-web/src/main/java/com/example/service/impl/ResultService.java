package com.example.service.impl;

import com.example.converter.ResultConverter;
import com.example.dto.ExaminationQuestionDTO;
import com.example.dto.ResultDTO;
import com.example.entity.ExaminationEntity;
import com.example.entity.ResultEntity;
import com.example.entity.UserEntity;
import com.example.repository.IExaminationRepository;
import com.example.repository.IResultRepository;
import com.example.repository.IUserRepository;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import com.example.service.IResultService;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.*;

public class ResultService implements IResultService {

	@Inject
	private IUserRepository userRepository;

	@Inject
	private IExaminationRepository examinationRepository;

	@Inject
	private IResultRepository resultRepository;

	@Inject
	private ResultConverter resultConverter;

	@Override
	public ResultDTO saveResult(String userName, Long examinationId, List<ExaminationQuestionDTO> examinationQuestions) {
		ResultDTO result = new ResultDTO();
		if (userName != null && examinationId != null && examinationQuestions != null) {
			UserEntity user = userRepository.findOneByProperty("name", userName);
			ExaminationEntity examination = examinationRepository.findById(examinationId);
			ResultEntity resultEntity = new ResultEntity();
			calculateCorrectNumberQuizOnline(resultEntity, examinationQuestions);
			initDataToResultEntity(resultEntity, user, examination);
			resultEntity = resultRepository.save(resultEntity);
			result = resultConverter.convertToDto(resultEntity);
		}
		return result;
	}

	@Override
	public List<ResultDTO> getResultsByUser(String userName, String examinationCode) {
		List<ResultDTO> resultDTOS = new ArrayList<>();
		UserEntity userEntity = userRepository.findOneByProperty("name", userName);
		List<ResultEntity> resultEntities = new ArrayList<>();
		if (examinationCode != null && StringUtils.isNotEmpty(examinationCode)) {
			ExaminationEntity examinationEntity = examinationRepository.findOneByProperty("code", examinationCode);
			Map<String, Object> properties = new HashMap<>();
			properties.put("user", userEntity);
			properties.put("examination", examinationEntity);
			resultEntities = resultRepository.findByProperty(properties);
		} else {
			resultEntities = resultRepository.findByProperty("user", userEntity);
		}
		for (ResultEntity item: resultEntities) {
			ResultDTO resultDTO = resultConverter.convertToDto(item);
			resultDTOS.add(resultDTO);
		}
		return resultDTOS;
	}

	private void calculateCorrectNumberQuizOnline(ResultEntity resultEntity, List<ExaminationQuestionDTO> examinationQuestions) {
		int correctNumber = 0;
		for (ExaminationQuestionDTO item: examinationQuestions) {
			if (item.getAnswerUsers() != null) {
				String[] correctAnswers = item.getCorrectAnswer().split(",");
				if (Arrays.equals(item.getAnswerUsers(), correctAnswers)) {
					correctNumber++;
				}
			}
		}
		resultEntity.setCorrectNumber(correctNumber);
		resultEntity.setTotalNumber(examinationQuestions.size());
	}

	private void initDataToResultEntity(ResultEntity resultEntity, UserEntity user, ExaminationEntity examination) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		resultEntity.setExamination(examination);
		resultEntity.setUser(user);
		resultEntity.setCreatedDate(timestamp);
	}

	@Override
	public List<ResultDTO> getResults() {
		List<ResultDTO> resultDTOS = new ArrayList<>();
		List<ResultEntity> resultEntities = resultRepository.findAll();
		for (ResultEntity item: resultEntities) {
			ResultDTO resultDTO = resultConverter.convertToDto(item);
			resultDTOS.add(resultDTO);
		}
		return resultDTOS;
	}

  @Override
  public List<ResultDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting) {
		List<ResultEntity> resultEntities = resultRepository.findAll(searchProperty, pagination, sorting);
		List<ResultDTO> resultDTOS = new ArrayList<>();
		for (ResultEntity item: resultEntities) {
			ResultDTO resultDTO = resultConverter.convertToDto(item);
			resultDTOS.add(resultDTO);
		}
		return resultDTOS;
  }

	@Override
	public int getTotalItem(Map<String, Object> searchProperty) {
		return Integer.parseInt(resultRepository.getTotalItem(searchProperty).toString());
	}
}
