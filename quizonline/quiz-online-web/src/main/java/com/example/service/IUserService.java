package com.example.service;

import com.example.dto.UserDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

import java.util.List;
import java.util.Map;

public interface IUserService {
	UserDTO findByName(String name);
	UserDTO findByNameAndPassword(String name, String password);
	UserDTO findById(Long id);
	void updateUser(UserDTO updateUser);
	UserDTO insertUser(UserDTO newUser);
	List<UserDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting);
	int getTotalItem(Map<String, Object> searchProperty);
	List<UserDTO> findAll();
}
