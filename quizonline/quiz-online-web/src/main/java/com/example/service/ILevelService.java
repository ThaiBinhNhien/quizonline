package com.example.service;

import java.util.List;
import java.util.Map;

import com.example.dto.CategoryDTO;
import com.example.dto.LevelDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

public interface ILevelService {
	List<LevelDTO> findAll();
	List<LevelDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting);
	List<LevelDTO> findAllIn(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting);
	int getTotalItem(Map<String, Object> searchProperty);
	LevelDTO findById(Long id);
	void deletById(Long id);
	void updateLevel(LevelDTO updateLevel);
	LevelDTO insertLevel(LevelDTO newLevel);
}
