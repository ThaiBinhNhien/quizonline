package com.example.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.example.converter.UserConverter;
import com.example.dto.UserDTO;
import com.example.entity.RoleEntity;
import com.example.entity.UserEntity;
import com.example.repository.IRoleRepository;
import com.example.repository.IUserRepository;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import com.example.service.IUserService;

public class UserService implements IUserService {

	@Inject
	private IUserRepository userRepository;

	@Inject
	private IRoleRepository roleRepository;

	@Inject
	private UserConverter userConverter;

	@Override
	public UserDTO findByName(String name) {
		UserEntity userEntity = userRepository.findOneByProperty("name", name);
		return userEntity != null ? userConverter.convertToDto(userEntity) : null;
	}

	@Override
	public UserDTO findByNameAndPassword(String name, String password) {
		UserEntity userEntity = userRepository.findOneByUserAndPass("name",name,"password", password);
		return userEntity != null ? userConverter.convertToDto(userEntity) : null;
	}
	
	@Override
	public UserDTO findById(Long id) {
		UserEntity userEntity = userRepository.findById(id);
		UserDTO userDTO = userConverter.convertToDto(userEntity);
		for (RoleEntity item: userEntity.getRoles()) {
			userDTO.setRoleCode(item.getCode());
		}
		return userDTO;
	}

	@Override
	public void updateUser(UserDTO updateUser) {
		UserEntity oldUser = userRepository.findById(updateUser.getId());
		oldUser.setName(updateUser.getName());
		oldUser.setFullName(updateUser.getFullName());
		oldUser.setPassword(updateUser.getPassword());
		if (updateUser.getRoleCode() != null) {
			RoleEntity role = roleRepository.findOneByProperty("code", updateUser.getRoleCode());
			List<RoleEntity> roles = new ArrayList<>();
			roles.add(role);
			oldUser.setRoles(roles);
		}
		userRepository.update(oldUser);
	}

	@Override
	public UserDTO insertUser(UserDTO newUser) {
		UserEntity user = userConverter.convertToEntity(newUser);
		String roleCode = "";
		if (newUser.getRoleCode() != null) {
			roleCode = newUser.getRoleCode();
		} else {
			roleCode = "USER";
		}
		RoleEntity role = roleRepository.findOneByProperty("code", roleCode);
		List<RoleEntity> roles = new ArrayList<>();
		roles.add(role);
		user.setRoles(roles);
		user.setStatus(1);
		user.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		return userConverter.convertToDto(userRepository.save(user));
	}

	@Override
	public List<UserDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting) {
		List<UserEntity> userEntities = userRepository.findAll(searchProperty, pagination, sorting);
		List<UserDTO> userDTOS = new ArrayList<UserDTO>();
		for (UserEntity item : userEntities) {
			UserDTO userDTO = userConverter.convertToDto(item);
			userDTOS.add(userDTO);
		}
		return userDTOS;
	}

	@Override
	public int getTotalItem(Map<String, Object> searchProperty) {
		return Integer.parseInt(userRepository.getTotalItem(searchProperty).toString());
	}

	@Override
	public List<UserDTO> findAll() {
		List<UserEntity> userEntities = userRepository.findAll();
		List<UserDTO> userDTOS = new ArrayList<UserDTO>();
		for (UserEntity item : userEntities) {
			UserDTO userDTO = userConverter.convertToDto(item);
			userDTOS.add(userDTO);
		}
		return userDTOS;
	}
}
