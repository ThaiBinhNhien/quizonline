package com.example.service.impl;

import com.example.converter.ExaminationConverter;
import com.example.dto.ExaminationDTO;
import com.example.entity.CategoryEntity;
import com.example.entity.ExaminationEntity;
import com.example.entity.LevelEntity;
import com.example.entity.UserEntity;
import com.example.repository.*;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import com.example.service.IExaminationService;
import com.example.utils.SessionUtil;
import org.apache.commons.collections.FastHashMap;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExaminationService implements IExaminationService {

	@Inject
	private IExaminationRepository examinationRepository;

	@Inject
	private ICategoryRepository categoryRepository;

	@Inject
	private ILevelRepository levelRepository;

	@Inject
	private ExaminationConverter examinationConverter;

	@Inject
	private IUserRepository userRepository;

	@Inject
	private IResultRepository resultRepository;

	@Override
	public ExaminationDTO findById(Long id) {
		ExaminationEntity examinationEntity = examinationRepository.findById(id);
		return examinationConverter.convertToDto(examinationEntity);
	}
	
	@Override
	public void deletById(Long id) {
		examinationRepository.delete(id);
	}

	@Override
	public void updateExamination(ExaminationDTO updateExamination) {
		ExaminationEntity oldExamination = examinationRepository.findById(updateExamination.getId());
		oldExamination.setName(updateExamination.getName());
		CategoryEntity category = categoryRepository.findOneByProperty("code", updateExamination.getCategoryCodeSelected());
		LevelEntity level = levelRepository.findOneByProperty("code", updateExamination.getLevelCodeSelected());
		oldExamination.setCategory(category);
		oldExamination.setLevel(level);
		oldExamination.setTime(updateExamination.getTime());
		examinationRepository.update(oldExamination);
	}

	@Override
	public ExaminationDTO insertExamination(ExaminationDTO newExamination, String name) {
		CategoryEntity category = categoryRepository.findOneByProperty("code", newExamination.getCategoryCodeSelected());
		LevelEntity level = levelRepository.findOneByProperty("code", newExamination.getLevelCodeSelected());
		ExaminationEntity examination = examinationConverter.convertToEntity(newExamination);
		examination.setCategory(category);
		examination.setLevel(level);
		examination.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		examination.setCreatedBy(name);
		return examinationConverter.convertToDto(examinationRepository.save(examination));
	}

	@Override
	public List<ExaminationDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting, String name) {
		List<ExaminationEntity> userEntities = examinationRepository.findAll(searchProperty, pagination, sorting);
		List<ExaminationDTO> examinationDTOS = new ArrayList<>();
		for (ExaminationEntity item : userEntities) {
			ExaminationDTO examinationDTO = examinationConverter.convertToDto(item);
			if (name != null) {
				UserEntity user = userRepository.findOneByProperty("name", name);
				Map<String, Object> properties = new HashMap<>();
				properties.put("user", user);
				properties.put("examination", item);
				examinationDTO.setCount(resultRepository.countByProperty(properties).intValue());
			}
			examinationDTOS.add(examinationDTO);
		}
		return examinationDTOS;
	}

	@Override
	public int getTotalItem(Map<String, Object> searchProperty) {
		return Integer.parseInt(examinationRepository.getTotalItem(searchProperty).toString());
	}

	@Override
	public List<ExaminationDTO> findAll() {
		List<ExaminationDTO> examinationDTOS = new ArrayList<>();
		List<ExaminationEntity> examinationEntities = examinationRepository.findAll();
		examinationEntities.forEach(item -> {
			examinationDTOS.add(examinationConverter.convertToDto(item));
		});
		return examinationDTOS;
	}
}
