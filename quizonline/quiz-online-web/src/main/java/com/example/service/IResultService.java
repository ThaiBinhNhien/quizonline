package com.example.service;

import com.example.dto.ExaminationQuestionDTO;
import com.example.dto.ResultDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

import java.util.List;
import java.util.Map;

public interface IResultService {
	ResultDTO saveResult(String userName, Long examinationId, List<ExaminationQuestionDTO> examinationQuestions);
	List<ResultDTO> getResultsByUser(String userName, String examinationCode);
	List<ResultDTO> getResults();
	List<ResultDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting);
	int getTotalItem(Map<String, Object> searchProperty);
}
