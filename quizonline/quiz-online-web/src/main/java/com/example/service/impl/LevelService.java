package com.example.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.example.converter.LevelConvert;
import com.example.dto.LevelDTO;
import com.example.entity.LevelEntity;
import com.example.repository.ILevelRepository;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import com.example.service.ILevelService;

public class LevelService implements ILevelService {

	@Inject
	private ILevelRepository levelRepository;

	@Inject
	private LevelConvert levelConvert;

	@Override
	public List<LevelDTO> findAll() {
		List<LevelDTO> result = new ArrayList<>();
		List<LevelEntity> objects = levelRepository.findAll();
		for (LevelEntity item: objects) {
			result.add(levelConvert.convertToDto(item));
		}
		return result;
	}
	
	@Override
	public List<LevelDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting) {
		List<LevelDTO> result = new ArrayList<>();
		List<LevelEntity> objects = levelRepository.findAll(searchProperty, pagination, sorting);
		for (LevelEntity item: objects) {
			result.add(levelConvert.convertToDto(item));
		}
		return result;
	}
	
	@Override
	public List<LevelDTO> findAllIn(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting) {
		List<LevelDTO> result = new ArrayList<>();
		List<LevelEntity> objects = levelRepository.findAllIn(searchProperty, pagination, sorting);
		for (LevelEntity item: objects) {
			result.add(levelConvert.convertToDto(item));
		}
		return result;
	}

	@Override
	public int getTotalItem(Map<String, Object> searchProperty) {
		return Integer.parseInt(levelRepository.getTotalItem(searchProperty).toString());
	}
	
	@Override
	public LevelDTO findById(Long id) {
		LevelEntity level = levelRepository.findById(id);
		LevelDTO userDTO = levelConvert.convertToDto(level);
		return userDTO;
	}
	
	@Override
	public void updateLevel(LevelDTO updateLevel) {
		LevelEntity oldCategory = levelRepository.findById(updateLevel.getId());
		oldCategory.setName(updateLevel.getName());
		oldCategory.setCode(updateLevel.getCode());
		oldCategory.setCreatedDate(updateLevel.getCreatedDate());
		oldCategory.setModifiedDate(new Timestamp(System.currentTimeMillis()));
		
		levelRepository.update(oldCategory);
	}
	
	@Override
	public void deletById(Long id) {
		levelRepository.delete(id);
	}

	@Override
	public LevelDTO insertLevel(LevelDTO newLevel) {
		LevelEntity levelEntity = levelConvert.convertToEntity(newLevel);
		levelEntity.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		levelEntity.setModifiedDate(new Timestamp(System.currentTimeMillis()));
		levelEntity.setName(newLevel.getName());
		levelEntity.setCode(newLevel.getCode());
		
		return levelConvert.convertToDto(levelRepository.save(levelEntity));
	}
}