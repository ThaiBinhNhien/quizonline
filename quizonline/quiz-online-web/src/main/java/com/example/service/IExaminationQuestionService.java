package com.example.service;

import com.example.dto.ExaminationQuestionDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

import java.util.List;
import java.util.Map;

public interface IExaminationQuestionService {
	List<ExaminationQuestionDTO> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting);
	List<ExaminationQuestionDTO> findAll(Map<String, Object> searchProperty, String name);
	int getTotalItem(Map<String, Object> searchProperty);
	void saveImport(List<ExaminationQuestionDTO> examinationQuestionDTOS, Long examinationId);
	void deleteAll(List<Long> examinationId);
	
}
