package com.example.converter;

import com.example.dto.ResultDTO;
import com.example.entity.ResultEntity;
import org.modelmapper.ModelMapper;

public class ResultConverter {

	private ModelMapper modelMapper;

	public ResultConverter() {
		modelMapper = new ModelMapper();
	}

	public ResultDTO convertToDto(ResultEntity entity) {
		ResultDTO result = modelMapper.map(entity, ResultDTO.class);
		return result;
	}

	public ResultEntity convertToEntity(ResultDTO dto) {
		ResultEntity result = modelMapper.map(dto, ResultEntity.class);
		return result;
	}
}
