package com.example.converter;

import org.modelmapper.ModelMapper;

import com.example.dto.LevelDTO;
import com.example.entity.LevelEntity;

public class LevelConvert {
	private ModelMapper modelMapper;
	
	public LevelConvert() {
		modelMapper = new ModelMapper();
	}
	
	public LevelDTO convertToDto(LevelEntity entity) {
		LevelDTO result = modelMapper.map(entity, LevelDTO.class);
		return result;
	}
	public LevelEntity convertToEntity(LevelDTO dto) {
		LevelEntity result = modelMapper.map(dto, LevelEntity.class);
		return result;
	}
}
