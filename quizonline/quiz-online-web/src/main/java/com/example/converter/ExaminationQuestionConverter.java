package com.example.converter;

import com.example.dto.ExaminationQuestionDTO;
import com.example.entity.ExaminationQuestionEntity;
import org.modelmapper.ModelMapper;

public class ExaminationQuestionConverter {

	private ModelMapper modelMapper;

	public ExaminationQuestionConverter() {
		modelMapper = new ModelMapper();
	}

	public ExaminationQuestionDTO convertToDto(ExaminationQuestionEntity entity) {
		ExaminationQuestionDTO result = modelMapper.map(entity, ExaminationQuestionDTO.class);
		return result;
	}

	public ExaminationQuestionEntity convertToEntity(ExaminationQuestionDTO dto) {
		ExaminationQuestionEntity result = modelMapper.map(dto, ExaminationQuestionEntity.class);
		return result;
	}
}
