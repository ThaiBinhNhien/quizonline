package com.example.converter;

import com.example.dto.ExaminationDTO;
import com.example.entity.ExaminationEntity;
import org.modelmapper.ModelMapper;

public class ExaminationConverter {

	private ModelMapper modelMapper;

	public ExaminationConverter() {
		modelMapper = new ModelMapper();
	}

	public ExaminationDTO convertToDto(ExaminationEntity entity) {
		ExaminationDTO result = modelMapper.map(entity, ExaminationDTO.class);
		return result;
	}

	public ExaminationEntity convertToEntity(ExaminationDTO dto) {
		ExaminationEntity result = modelMapper.map(dto, ExaminationEntity.class);
		return result;
	}
}
