package com.example.converter;

import com.example.dto.CategoryDTO;
import com.example.entity.CategoryEntity;
import org.modelmapper.ModelMapper;

public class CategoryConverter {

	private ModelMapper modelMapper;

	public CategoryConverter() {
		modelMapper = new ModelMapper();
	}

	public CategoryDTO convertToDto(CategoryEntity entity) {
		CategoryDTO result = modelMapper.map(entity, CategoryDTO.class);
		return result;
	}

	public CategoryEntity convertToEntity(CategoryDTO dto) {
		CategoryEntity result = modelMapper.map(dto, CategoryEntity.class);
		return result;
	}
}
