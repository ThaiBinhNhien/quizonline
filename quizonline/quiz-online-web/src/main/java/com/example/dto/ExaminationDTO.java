package com.example.dto;

public class ExaminationDTO extends AbstractDTO<ExaminationDTO> {

	private static final long serialVersionUID = 9201203238394851370L;

	private String name;
	private String code;
	private String createdBy;
	private int time;
	private int maxIteration;
	private int count;
	private CategoryDTO category;
	private LevelDTO level;
	private String levelCodeSelected;
	private String categoryCodeSelected;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getMaxIteration() {
		return maxIteration;
	}

	public void setMaxIteration(int maxIteration) {
		this.maxIteration = maxIteration;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CategoryDTO getCategory() {
		return category;
	}

	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	public LevelDTO getLevel() {
		return level;
	}

	public void setLevel(LevelDTO level) {
		this.level = level;
	}

	public String getLevelCodeSelected() {
		return levelCodeSelected;
	}

	public void setLevelCodeSelected(String levelCodeSelected) {
		this.levelCodeSelected = levelCodeSelected;
	}

	public String getCategoryCodeSelected() {
		return categoryCodeSelected;
	}

	public void setCategoryCodeSelected(String categoryCodeSelected) {
		this.categoryCodeSelected = categoryCodeSelected;
	}
}
