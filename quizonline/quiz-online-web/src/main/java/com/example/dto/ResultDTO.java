package com.example.dto;

import java.util.List;

public class ResultDTO extends AbstractDTO<ResultDTO> {

	private static final long serialVersionUID = 3782670018910229519L;

	private Integer correctNumber;
	private Integer totalNumber;
	private ExaminationDTO examination;
	private UserDTO user;
	private List<ExaminationDTO> examinations;
	private List<UserDTO> users;
	private String[] names;
	private String examinationCode;
	private String fromDate;
	private String toDate;

	public Integer getCorrectNumber() {
		return correctNumber;
	}

	public void setCorrectNumber(Integer correctNumber) {
		this.correctNumber = correctNumber;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}

	public ExaminationDTO getExamination() {
		return examination;
	}

	public void setExamination(ExaminationDTO examination) {
		this.examination = examination;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public List<ExaminationDTO> getExaminations() {
		return examinations;
	}

	public void setExaminations(List<ExaminationDTO> examinations) {
		this.examinations = examinations;
	}

	public List<UserDTO> getUsers() {
		return users;
	}

	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}

	public String[] getNames() {
		return names;
	}

	public void setNames(String[] names) {
		this.names = names;
	}

	public String getExaminationCode() {
		return examinationCode;
	}

	public void setExaminationCode(String examinationCode) {
		this.examinationCode = examinationCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
}
