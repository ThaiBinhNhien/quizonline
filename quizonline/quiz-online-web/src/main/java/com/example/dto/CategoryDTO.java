package com.example.dto;

public class CategoryDTO extends AbstractDTO<CategoryDTO> {

	private static final long serialVersionUID = -4553583583100510783L;

	private String name;
	private String code;
	private String levels;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}
	
}
