package com.example.dto;

public class LevelDTO extends AbstractDTO<LevelDTO> {

	private static final long serialVersionUID = -3043340918380688784L;

	private Long id;
	private String name;
	private String code;


	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
}
