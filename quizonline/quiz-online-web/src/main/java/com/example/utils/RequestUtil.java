package com.example.utils;

import com.example.dto.AbstractDTO;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import org.apache.commons.lang.StringUtils;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;

import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

	public static void initSearchBean(HttpServletRequest request, AbstractDTO bean) {
		if (bean != null) {
			String sortExpression = request.getParameter(new ParamEncoder(bean.getTableId()).encodeParameterName(TableTagParameters.PARAMETER_SORT));
			String sortDirection = request.getParameter(new ParamEncoder(bean.getTableId()).encodeParameterName(TableTagParameters.PARAMETER_ORDER));
			String pageStr = request.getParameter(new ParamEncoder(bean.getTableId()).encodeParameterName(TableTagParameters.PARAMETER_PAGE));

			Integer page = 1;
			if (StringUtils.isNotBlank(pageStr)) {
				try {
					page = Integer.valueOf(pageStr);
				} catch (Exception e) {
					//ignore
				}
			}
			bean.setPage(page);
			bean.setSortDirection(sortDirection);
			bean.setSortExpression(sortExpression);
			bean.setFirstItem((bean.getPage() - 1) * bean.getMaxPageItems());

			Pagination pagination = new Pagination();
			pagination.setOffset(bean.getFirstItem());
			pagination.setLimit(bean.getMaxPageItems());
			bean.setPagination(pagination);

			Sorting sorting = new Sorting();
			sorting.setSortExpression(bean.getSortExpression());
			sorting.setSortDirection(bean.getSortDirection());
			bean.setSorting(sorting);
		}
	}

	public static void initSearchBeanManual(AbstractDTO bean) {
		if (bean != null) {
			Integer page = 1;
			if (bean.getPage() != 0) {
				page = bean.getPage();
			}
			bean.setPage(page);
			bean.setFirstItem((bean.getPage()-1) * bean.getMaxPageItems());

			Pagination pagination = new Pagination();
			pagination.setOffset(bean.getFirstItem());
			pagination.setLimit(bean.getMaxPageItems());
			bean.setPagination(pagination);

			Sorting sorting = new Sorting();
			sorting.setSortExpression(bean.getSortExpression());
			sorting.setSortDirection(bean.getSortDirection());
			bean.setSorting(sorting);
		}
	}
}
