package com.example.repository.impl;

import com.example.entity.LevelEntity;
import com.example.repository.ILevelRepository;

public class LevelRepository extends AbstractRepository<Long, LevelEntity> implements ILevelRepository {
}
