package com.example.repository.impl;

import com.example.entity.ExaminationEntity;
import com.example.entity.ExaminationQuestionEntity;
import com.example.repository.IExaminationQuestionRepository;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class ExaminationQuestionRepository extends AbstractRepository<Long, ExaminationQuestionEntity> implements IExaminationQuestionRepository{

	@Override
	public List<ExaminationQuestionEntity> findAllQuestion(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting, String value) {
		String addCondition = null;
		if (value != null && StringUtils.isNotEmpty(value)) {
			addCondition = " AND ((LOWER(question) LIKE '%"+value+"%') OR (LOWER(option1) LIKE '%"+value+"%') OR (LOWER(option2) LIKE '%"+value+"%') OR (LOWER(option3) LIKE '%"+value+"%') OR (LOWER(option4) LIKE '%"+value+"%')) ";
		}
		if (addCondition == null) {
			return findAll(searchProperty, pagination, sorting);
		} else {
			return findAll(searchProperty, pagination, sorting, addCondition);
		}
	}
	@Override
	public void deleteAllByExamination(Object idExamination) {
		
		deleteAllByID("examination", idExamination);
	}
	@Override
	public Object getTotalItemQuestion(Map<String, Object> searchProperty, String value) {
		String addCondition = null;
		if (value != null && StringUtils.isNotEmpty(value)) {
			addCondition = " OR (LOWER(question) LIKE '%"+value+"%') OR (LOWER(option1) LIKE '%"+value+"%') OR (LOWER(option2) LIKE '%"+value+"%') OR (LOWER(option3) LIKE '%"+value+"%') OR (LOWER(option4) LIKE '%"+value+"%') ";
		}
		if (addCondition == null) {
			return getTotalItem(searchProperty);
		} else {
			return getTotalItem(searchProperty, addCondition, addCondition);
		}
	}
}
