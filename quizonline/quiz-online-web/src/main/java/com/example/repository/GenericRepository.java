package com.example.repository;

import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface GenericRepository<ID extends Serializable, T> {
	List<T> findAll();
	T update(T entity);
	T save(T entity);
	T findById(ID id);
	Integer delete(List<ID> ids);
	void delete(ID id);
	T findOneByProperty(String property, Object value);
	T findOneByUserAndPass(String user, Object valueUser, String pass, Object valuePass);
	void deleteAllByID(String key, Object value);
	List<T> findByProperty(String propertyName, Object value, int... rowStartIdxAndCount);
	List<T> findByProperty(Map<String, Object> properties);
	List<T> findAll(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting, String... addCondition);
	List<T> findAllIn(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting, String... addCondition);
	Object getTotalItem(Map<String, Object> searchProperty, String... addCondition);
	Long countByProperty(Map<String, Object> searchProperty);
}
