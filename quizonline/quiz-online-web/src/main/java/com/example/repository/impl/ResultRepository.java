package com.example.repository.impl;

import com.example.entity.ResultEntity;
import com.example.entity.UserEntity;
import com.example.repository.IResultRepository;
import com.example.repository.IUserRepository;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ResultRepository extends AbstractRepository<Long, ResultEntity> implements IResultRepository {

	@Inject
	private IUserRepository userRepository;

	@Override
	public List<ResultEntity> findByNameAndExaminationAndCreatedDate(Map<String, Object> mapProperties, Timestamp fromDate, Timestamp toDate) {
		StringBuilder whereClause = new StringBuilder(" AND 1=1 ");
		if (fromDate != null) {
			whereClause.append(" AND DATE_FORMAT(createdDate, '%Y-%m-%d') >= DATE_FORMAT('"+fromDate+"', '%Y-%m-%d') ");
		}
		if (toDate != null) {
			whereClause.append(" AND DATE_FORMAT(createdDate, '%Y-%m-%d') <= DATE_FORMAT('"+toDate+"', '%Y-%m-%d') ");
		}
		return this.findAll(mapProperties, new Pagination(), new Sorting(), whereClause.toString());
	}
}
