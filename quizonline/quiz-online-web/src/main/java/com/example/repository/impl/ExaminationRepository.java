package com.example.repository.impl;

import com.example.entity.ExaminationEntity;
import com.example.repository.IExaminationRepository;

public class ExaminationRepository extends AbstractRepository<Long, ExaminationEntity> implements IExaminationRepository {
}
