package com.example.repository;

import com.example.entity.ExaminationEntity;

public interface IExaminationRepository extends GenericRepository<Long, ExaminationEntity> {
}
