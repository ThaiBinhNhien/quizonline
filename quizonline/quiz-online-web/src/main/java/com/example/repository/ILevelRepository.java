package com.example.repository;

import com.example.entity.LevelEntity;

public interface ILevelRepository extends GenericRepository<Long, LevelEntity> {
}
