package com.example.repository;

import java.util.List;
import java.util.Map;

import com.example.entity.ExaminationQuestionEntity;
import com.example.repository.beans.Pagination;
import com.example.repository.beans.Sorting;

public interface IExaminationQuestionRepository extends GenericRepository<Long, ExaminationQuestionEntity> {
	List<ExaminationQuestionEntity> findAllQuestion(Map<String, Object> searchProperty, Pagination pagination, Sorting sorting, String value);
	Object getTotalItemQuestion(Map<String, Object> searchProperty, String value);
	void deleteAllByExamination(Object idExamination);
}
