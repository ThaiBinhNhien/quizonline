package com.example.repository;

import com.example.entity.RoleEntity;

public interface IRoleRepository extends GenericRepository<Long, RoleEntity> {
}
