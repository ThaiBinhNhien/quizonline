package com.example.repository;

import com.example.entity.UserEntity;

public interface IUserRepository extends GenericRepository<Long, UserEntity> {
}
