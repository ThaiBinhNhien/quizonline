package com.example.repository;

import com.example.entity.CategoryEntity;

public interface ICategoryRepository extends GenericRepository<Long, CategoryEntity> {
}
