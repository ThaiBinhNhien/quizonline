package com.example.repository.impl;

import com.example.entity.RoleEntity;
import com.example.repository.IRoleRepository;

public class RoleRepository extends AbstractRepository<Long, RoleEntity> implements IRoleRepository {
}
