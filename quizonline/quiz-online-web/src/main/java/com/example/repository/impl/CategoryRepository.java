package com.example.repository.impl;

import com.example.entity.CategoryEntity;
import com.example.repository.ICategoryRepository;

public class CategoryRepository extends AbstractRepository<Long, CategoryEntity> implements ICategoryRepository {
}
