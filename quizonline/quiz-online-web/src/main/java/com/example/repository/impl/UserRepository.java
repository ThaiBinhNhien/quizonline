package com.example.repository.impl;

import com.example.entity.UserEntity;
import com.example.repository.IUserRepository;

public class UserRepository extends AbstractRepository<Long, UserEntity> implements IUserRepository {
}
