package com.example.repository;

import com.example.entity.ResultEntity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public interface IResultRepository extends GenericRepository<Long, ResultEntity> {
	List<ResultEntity> findByNameAndExaminationAndCreatedDate(Map<String, Object> mapProperties, Timestamp fromDate, Timestamp toDate);
}
