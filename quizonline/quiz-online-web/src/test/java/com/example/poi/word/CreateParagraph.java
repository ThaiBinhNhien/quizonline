package com.example.poi.word;

import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CreateParagraph {

	public static void main(String[] args)throws Exception {
		//Blank Document
		XWPFDocument document = new XWPFDocument();

		//Write the Document in file system
		try(FileOutputStream out = new FileOutputStream(new File("/poi/word/createparagraph.docx"))) {

			XWPFParagraph paragraph1 = document.createParagraph();
			XWPFRun run1 = paragraph1.createRun();
			run1.setText("examination question 1");
			run1.addBreak();
			run1.setText("A. Option 1");
			run1.addBreak();
			run1.setText("B. Option 1.2");

			XWPFParagraph paragraph2 = document.createParagraph();
			XWPFRun run2 = paragraph2.createRun();
			run2.setText("examination question 2");
			run2.addBreak();
			run2.setText("A. Option 1");
			run2.addBreak();
			run2.setText("B. Option 1.2");

			document.write(out);
			System.out.println("createparagraph.docx written successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}