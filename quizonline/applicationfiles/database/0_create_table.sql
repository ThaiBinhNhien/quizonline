use quizonline;

CREATE TABLE user(
  id bigint NOT NULL PRIMARY KEY auto_increment,
  name VARCHAR(150) NOT NULL,
  password VARCHAR(150) NOT NULL,
  fullname VARCHAR(150) NULL,
  status int NOT NULL,
  createddate TIMESTAMP NULL,
  modifieddate TIMESTAMP NULL
);

CREATE TABLE role(
  id bigint NOT NULL PRIMARY KEY auto_increment,
  name VARCHAR(150) NOT NULL,
  code VARCHAR(150) NOT NULL,
  createddate TIMESTAMP NULL,
  modifieddate TIMESTAMP NULL
);

CREATE TABLE user_role(
  id bigint NOT NULL PRIMARY KEY auto_increment,
  userid bigint NOT NULL,
  roleid bigint NOT NULL,
  createddate TIMESTAMP NULL,
  modifieddate TIMESTAMP NULL
);

ALTER TABLE user_role ADD CONSTRAINT fk_userrole_user
FOREIGN KEY (userid) REFERENCES user(id);

ALTER TABLE user_role ADD CONSTRAINT fk_userrole_role
FOREIGN KEY (roleid) REFERENCES role(id);

CREATE TABLE examination (
  id bigint NOT NULL PRIMARY KEY auto_increment,
  name VARCHAR(255) NOT NULL,
  createddate TIMESTAMP NULL,
  modifieddate TIMESTAMP NULL,
  createdby VARCHAR(255),
  categorycode VARCHAR(255),
  levelcode VARCHAR(255),
  unique(name)
);

CREATE TABLE category (
  id bigint NOT NULL PRIMARY KEY auto_increment,
  name VARCHAR(255) NOT NULL,
  code VARCHAR(255),
  createddate TIMESTAMP NULL,
  modifieddate TIMESTAMP NULL,
  unique(name)
);

CREATE TABLE level (
  id bigint NOT NULL PRIMARY KEY auto_increment,
  name VARCHAR(255) NOT NULL,
  code VARCHAR(255),
  createddate TIMESTAMP NULL,
  modifieddate TIMESTAMP NULL,
  unique(name)
);

CREATE TABLE examinationquestion (
  id bigint NOT NULL PRIMARY KEY auto_increment,
  question TEXT NOT NULL,
  option1 TEXT NOT NULL,
  option2 TEXT NOT NULL,
  option3 TEXT NOT NULL,
  option4 TEXT NOT NULL,
  correctanswer TEXT NOT NULL,
  examinationid BIGINT NOT NULL,
  createddate TIMESTAMP NULL,
  modifieddate TIMESTAMP NULL
);

ALTER TABLE examinationquestion ADD CONSTRAINT fk_examinationquestion_examination FOREIGN KEY (examinationid) REFERENCES examination(id);

CREATE TABLE result (
  id bigint NOT NULL PRIMARY KEY auto_increment,
  correctnumber INT NOT NULL,
  totalnumber INT NOT NULL,
  examinationid BIGINT NOT NULL,
  userid BIGINT NOT NULL,
  createddate TIMESTAMP null,
  modifieddate TIMESTAMP null
);

ALTER TABLE result ADD CONSTRAINT fk_result_examination FOREIGN KEY (examinationid) REFERENCES examination(id);
ALTER TABLE result ADD CONSTRAINT fk_result_user FOREIGN KEY (userid) REFERENCES user(id);
















