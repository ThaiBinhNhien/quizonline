use quizonline;

insert into role(code,name) values('ADMIN','Quản trị hệ thống');
insert into role(code,name) values('USER','người dùng');
insert into role(code,name) values('MANAGER','Quản lý');

insert into user(name,password,fullname,status)
values('admin','123456','admin',1);
insert into user(name,password,fullname,status)
values('nguyenvana','123456','nguyen van a',1);
insert into user(name,password,fullname,status)
values('manager1','123456','manager 1',1);
insert into user(name,password,fullname,status)
values('manager2','123456','manager 2',1);

INSERT INTO user_role(userid,roleid) VALUES (1,1);
INSERT INTO user_role(userid,roleid) VALUES (2,2);
INSERT INTO user_role(userid,roleid) VALUES (3,3);
INSERT INTO user_role(userid,roleid) VALUES (4,3);

insert into category(code,name) values('HOA','Hóa Học');
insert into category(code,name) values('TOAN','Toán');
insert into category(code,name) values('VATLY','Vật lý');

insert into level(code,name) values('LEVEL10','Lớp 10');
insert into level(code,name) values('LEVEL11','Lớp 11');
insert into level(code,name) values('LEVEL12','Lớp 12');

