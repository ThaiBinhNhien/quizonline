/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.21-MariaDB : Database - quizonline
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`quizonline` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `quizonline`;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`code`,`createddate`,`modifieddate`) values (1,'Hóa Học','HOA',NULL,NULL),(2,'Toán','TOAN',NULL,NULL),(3,'Vật lý','VATLY',NULL,NULL);

/*Table structure for table `examination` */

DROP TABLE IF EXISTS `examination`;

CREATE TABLE `examination` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `maxiteration` int(11) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `categoryid` bigint(20) DEFAULT NULL,
  `levelid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_examination_category_idx` (`categoryid`),
  KEY `fk_examination_level_idx` (`levelid`),
  CONSTRAINT `fk_examination_category` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_examination_level` FOREIGN KEY (`levelid`) REFERENCES `level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `examination` */

insert  into `examination`(`id`,`name`,`createddate`,`modifieddate`,`createdby`,`time`,`maxiteration`,`code`,`categoryid`,`levelid`) values (3,'Đề thi toán lớp 10','2018-05-31 15:47:48',NULL,'manager1',60,100,'DETHITOANLOP10',2,1),(4,'Đề thi toán lớp 11','2018-06-05 08:27:46',NULL,'admin',2,3,'DETHITOANLOP11',1,2),(5,'Đề thi lý 12','2018-06-05 19:25:46',NULL,'quanly1',1,2,'DETHITOANLOP12',3,3),(6,'Test','2019-03-31 00:21:20',NULL,'admin',120,2,'K001',1,3),(7,'ÄÃªÌ thi thÆ°Ì','2019-04-01 01:59:59',NULL,'admin',2,2,'THITHULOP10',1,1),(8,'Thi thử Vật lý 12','2019-04-01 02:05:13',NULL,'admin',10,1,'THITHUVL12',3,3),(9,'Thi thủ toán 11','2019-04-01 02:11:27',NULL,'admin',12,12,'NCHJFUD',2,1),(10,'Thi Hóa Lớp 10','2019-04-01 02:29:49',NULL,'admin',12,12,'HOA10',1,2);

/*Table structure for table `examinationquestion` */

DROP TABLE IF EXISTS `examinationquestion`;

CREATE TABLE `examinationquestion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `option1` text NOT NULL,
  `option2` text NOT NULL,
  `option3` text NOT NULL,
  `option4` text NOT NULL,
  `correctanswer` text NOT NULL,
  `examinationid` bigint(20) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  `suggest` text,
  PRIMARY KEY (`id`),
  KEY `fk_examinationquestion_examination` (`examinationid`),
  CONSTRAINT `fk_examinationquestion_examination` FOREIGN KEY (`examinationid`) REFERENCES `examination` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `examinationquestion` */

insert  into `examinationquestion`(`id`,`question`,`option1`,`option2`,`option3`,`option4`,`correctanswer`,`examinationid`,`createddate`,`modifieddate`,`suggest`) values (5,'examination question 1','A. Option 1','B. Option 1.2','C. Option 1.3','D. Option 1.4','A,B,C',3,'2018-06-05 00:25:44',NULL,'gợi ý 1'),(6,'examination question 2','A. Option 2.1','B. Option 2.2','C. Option 2.3','D. Option 2.4','B,C,D',3,'2018-06-05 00:25:44',NULL,'gợi ý 2'),(7,'examination question 3','A. Option 3.1','B. Option 3.2','C. Option 3.3','D. Option 3.4','C',3,'2018-06-05 00:25:44',NULL,'truong tung lam'),(8,'examination question 4','A. Option 4.1','B. Option 4.2','C. Option 4.3','D. Option 5.4','D',3,'2018-06-05 00:25:44',NULL,'truong tung lam'),(9,'examination question 1','A. Option 1','B. Option 1.2','C. Option 1.3','D. Option 1.4','A',4,'2018-06-05 08:28:20',NULL,'truong tung lam'),(10,'examination question 2','A. Option 2.1','B. Option 2.2','C. Option 2.3','D. Option 2.4','B',4,'2018-06-05 08:28:20',NULL,'truong tung lam'),(11,'examination question 3','A. Option 3.1','B. Option 3.2','C. Option 3.3','D. Option 3.4','C',4,'2018-06-05 08:28:20',NULL,'truong tung lam'),(12,'examination question 4','A. Option 4.1','B. Option 4.2','C. Option 4.3','D. Option 5.4','D',4,'2018-06-05 08:28:20',NULL,'truong tung lam'),(13,'examination question 1','A. Option 1','B. Option 1.2','C. Option 1.3','D. Option 1.4','A',5,'2018-06-05 19:29:41',NULL,'truong tung lam'),(14,'examination question 2','A. Option 2.1','B. Option 2.2','C. Option 2.3','D. Option 2.4','B',5,'2018-06-05 19:29:41',NULL,'truong tung lam'),(15,'examination question 3','A. Option 3.1','B. Option 3.2','C. Option 3.3','D. Option 3.4','C',5,'2018-06-05 19:29:41',NULL,'truong tung lam'),(16,'examination question 4','A. Option 4.1','B. Option 4.2','C. Option 4.3','D. Option 5.4','D',5,'2018-06-05 19:29:41',NULL,'truong tung lam'),(17,'examination question 1','A. Option 1','B. Option 1.2','C. Option 1.3','D. Option 1.4','A',3,'2019-03-31 00:26:41',NULL,''),(18,'examination question 2','A. Option 2.1','B. Option 2.2','C. Option 2.3','D. Option 2.4','B',3,'2019-03-31 00:26:41',NULL,''),(19,'examination question 3','A. Option 3.1','B. Option 3.2','C. Option 3.3','D. Option 3.4','C',3,'2019-03-31 00:26:41',NULL,''),(20,'examination question 4','A. Option 4.1','B. Option 4.2','C. Option 4.3','D. Option 5.4','D',3,'2019-03-31 00:26:41',NULL,'');

/*Table structure for table `level` */

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `level` */

insert  into `level`(`id`,`name`,`code`,`createddate`,`modifieddate`) values (1,'Lớp 10','LEVEL10',NULL,NULL),(2,'Lớp 11','LEVEL11',NULL,NULL),(3,'Lớp 12','LEVEL12',NULL,NULL);

/*Table structure for table `result` */

DROP TABLE IF EXISTS `result`;

CREATE TABLE `result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `correctnumber` int(11) NOT NULL,
  `totalnumber` int(11) NOT NULL,
  `examinationid` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_result_examination` (`examinationid`),
  KEY `fk_result_user` (`userid`),
  CONSTRAINT `fk_result_examination` FOREIGN KEY (`examinationid`) REFERENCES `examination` (`id`),
  CONSTRAINT `fk_result_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

/*Data for the table `result` */

insert  into `result`(`id`,`correctnumber`,`totalnumber`,`examinationid`,`userid`,`createddate`,`modifieddate`) values (9,2,4,3,2,'2018-06-05 00:38:17',NULL),(10,0,4,3,2,'2018-06-05 00:39:27',NULL),(11,0,4,3,2,'2018-06-07 00:43:07',NULL),(12,1,4,4,2,'2018-06-07 08:28:50',NULL),(13,1,4,4,2,'2018-06-08 08:29:11',NULL),(14,3,4,3,8,'2018-06-08 19:35:35',NULL),(15,0,4,3,8,'2018-06-10 19:38:27',NULL),(16,0,4,5,8,'2018-06-18 08:20:07',NULL),(17,0,4,5,8,'2018-06-18 08:34:16',NULL),(18,1,4,3,8,'2018-06-18 08:34:50',NULL),(19,0,4,3,8,'2018-06-18 23:49:28',NULL),(20,0,4,3,8,'2018-06-18 23:51:47',NULL),(21,0,4,3,8,'2018-06-19 00:09:02',NULL),(22,0,4,3,8,'2018-06-19 00:11:03',NULL),(23,0,4,3,8,'2018-06-19 00:14:17',NULL),(24,2,4,3,8,'2018-06-23 15:36:20',NULL),(25,0,4,3,8,'2018-06-23 15:37:28',NULL),(26,0,8,3,1,'2019-03-31 00:30:01',NULL),(27,0,8,3,1,'2019-03-31 00:53:31',NULL),(28,0,4,4,11,'2019-03-31 01:13:23',NULL);

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `code` varchar(150) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`id`,`name`,`code`,`createddate`,`modifieddate`) values (1,'Quản trị hệ thống','ADMIN',NULL,NULL),(2,'người dùng','USER',NULL,NULL),(3,'Quản lý','MANAGER',NULL,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `fullname` varchar(150) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`password`,`fullname`,`status`,`createddate`,`modifieddate`) values (1,'admin','123456','admin',1,NULL,NULL),(2,'nguyenvana','123456','nguyen van a',1,NULL,NULL),(3,'manager1','123456','manager 1',1,NULL,NULL),(4,'manager2','123456','manager 2',1,NULL,NULL),(5,'nguyenvanb','456789','nguyen van b',1,'2018-05-29 23:32:09',NULL),(6,'manager3','123456','manager3',1,'2018-05-31 10:42:19',NULL),(7,'manager4','123456','manager4',1,'2018-05-31 10:42:44',NULL),(8,'danglehuythai','456789','dang le huy thai',1,'2018-06-05 19:16:17',NULL),(9,'quanly1','123456','quan ly 1',1,'2018-06-05 19:19:10',NULL),(10,'quanly2','123456','quan ly 2',1,'2018-06-05 19:20:39',NULL),(11,'1','Khongnho!1','Thái Binh Nhiên',1,'2019-03-31 00:04:38',NULL),(12,'nhien_tb','Khongnho!','ThaÌi BiÌnh NhiÃªn',1,'2019-04-01 01:52:57',NULL);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `roleid` bigint(20) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_userrole_user` (`userid`),
  KEY `fk_userrole_role` (`roleid`),
  CONSTRAINT `fk_userrole_role` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_userrole_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`id`,`userid`,`roleid`,`createddate`,`modifieddate`) values (1,1,1,NULL,NULL),(3,3,3,NULL,NULL),(4,4,3,NULL,NULL),(5,5,2,NULL,NULL),(10,2,2,NULL,NULL),(11,6,3,NULL,NULL),(12,7,3,NULL,NULL),(13,8,2,NULL,NULL),(16,9,3,NULL,NULL),(17,10,3,NULL,NULL),(18,11,2,NULL,NULL),(19,12,2,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
