CREATE DATABASE  IF NOT EXISTS `quizonline` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `quizonline`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: quizonline
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Hóa Học','HOA',NULL,NULL),(2,'Toán','TOAN',NULL,NULL),(3,'Vật lý','VATLY',NULL,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examination`
--

DROP TABLE IF EXISTS `examination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examination` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `maxiteration` int(11) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `categoryid` bigint(20) DEFAULT NULL,
  `levelid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examination`
--

LOCK TABLES `examination` WRITE;
/*!40000 ALTER TABLE `examination` DISABLE KEYS */;
INSERT INTO `examination` VALUES (3,'Đề thi toán lớp 10','2018-05-31 08:47:48',NULL,'manager1',60,100,'DETHITOANLOP10',2,1),(4,'Đề thi hóa 11','2018-06-05 01:27:46',NULL,'admin',2,3,'DETHITOANLOP11',1,2),(5,'Đề thi lý 12','2018-06-05 12:25:46',NULL,'quanly1',1,2,'DETHITOANLOP12',3,3);
/*!40000 ALTER TABLE `examination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examinationquestion`
--

DROP TABLE IF EXISTS `examinationquestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examinationquestion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `option1` text NOT NULL,
  `option2` text NOT NULL,
  `option3` text NOT NULL,
  `option4` text NOT NULL,
  `correctanswer` text NOT NULL,
  `examinationid` bigint(20) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  `suggest` text,
  PRIMARY KEY (`id`),
  KEY `fk_examinationquestion_examination` (`examinationid`),
  CONSTRAINT `fk_examinationquestion_examination` FOREIGN KEY (`examinationid`) REFERENCES `examination` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examinationquestion`
--

LOCK TABLES `examinationquestion` WRITE;
/*!40000 ALTER TABLE `examinationquestion` DISABLE KEYS */;
INSERT INTO `examinationquestion` VALUES (5,'examination question 1','A. Option 1','B. Option 1.2','C. Option 1.3','D. Option 1.4','A,B,C',3,'2018-06-04 17:25:44',NULL,'gợi ý 1'),(6,'examination question 2','A. Option 2.1','B. Option 2.2','C. Option 2.3','D. Option 2.4','B,C,D',3,'2018-06-04 17:25:44',NULL,'gợi ý 2'),(7,'examination question 3','A. Option 3.1','B. Option 3.2','C. Option 3.3','D. Option 3.4','C',3,'2018-06-04 17:25:44',NULL,'truong tung lam'),(8,'examination question 4','A. Option 4.1','B. Option 4.2','C. Option 4.3','D. Option 5.4','D',3,'2018-06-04 17:25:44',NULL,'truong tung lam'),(9,'examination question 1','A. Option 1','B. Option 1.2','C. Option 1.3','D. Option 1.4','A',4,'2018-06-05 01:28:20',NULL,'truong tung lam'),(10,'examination question 2','A. Option 2.1','B. Option 2.2','C. Option 2.3','D. Option 2.4','B',4,'2018-06-05 01:28:20',NULL,'truong tung lam'),(11,'examination question 3','A. Option 3.1','B. Option 3.2','C. Option 3.3','D. Option 3.4','C',4,'2018-06-05 01:28:20',NULL,'truong tung lam'),(12,'examination question 4','A. Option 4.1','B. Option 4.2','C. Option 4.3','D. Option 5.4','D',4,'2018-06-05 01:28:20',NULL,'truong tung lam'),(13,'examination question 1','A. Option 1','B. Option 1.2','C. Option 1.3','D. Option 1.4','A',5,'2018-06-05 12:29:41',NULL,'truong tung lam'),(14,'examination question 2','A. Option 2.1','B. Option 2.2','C. Option 2.3','D. Option 2.4','B',5,'2018-06-05 12:29:41',NULL,'truong tung lam'),(15,'examination question 3','A. Option 3.1','B. Option 3.2','C. Option 3.3','D. Option 3.4','C',5,'2018-06-05 12:29:41',NULL,'truong tung lam'),(16,'examination question 4','A. Option 4.1','B. Option 4.2','C. Option 4.3','D. Option 5.4','D',5,'2018-06-05 12:29:41',NULL,'truong tung lam');
/*!40000 ALTER TABLE `examinationquestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `level`
--

LOCK TABLES `level` WRITE;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` VALUES (1,'Lớp 10','LEVEL10',NULL,NULL),(2,'Lớp 11','LEVEL11',NULL,NULL),(3,'Lớp 12','LEVEL12',NULL,NULL);
/*!40000 ALTER TABLE `level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `correctnumber` int(11) NOT NULL,
  `totalnumber` int(11) NOT NULL,
  `examinationid` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_result_examination` (`examinationid`),
  KEY `fk_result_user` (`userid`),
  CONSTRAINT `fk_result_examination` FOREIGN KEY (`examinationid`) REFERENCES `examination` (`id`),
  CONSTRAINT `fk_result_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (9,2,4,3,2,'2018-06-04 17:38:17',NULL),(10,0,4,3,2,'2018-06-04 17:39:27',NULL),(11,0,4,3,2,'2018-06-06 17:43:07',NULL),(12,1,4,4,2,'2018-06-07 01:28:50',NULL),(13,1,4,4,2,'2018-06-08 01:29:11',NULL),(14,3,4,3,8,'2018-06-08 12:35:35',NULL),(15,0,4,3,8,'2018-06-10 12:38:27',NULL),(16,0,4,5,8,'2018-06-18 01:20:07',NULL),(17,0,4,5,8,'2018-06-18 01:34:16',NULL),(18,1,4,3,8,'2018-06-18 01:34:50',NULL),(19,0,4,3,8,'2018-06-18 16:49:28',NULL),(20,0,4,3,8,'2018-06-18 16:51:47',NULL),(21,0,4,3,8,'2018-06-18 17:09:02',NULL),(22,0,4,3,8,'2018-06-18 17:11:03',NULL),(23,0,4,3,8,'2018-06-18 17:14:17',NULL),(24,2,4,3,8,'2018-06-23 08:36:20',NULL),(25,0,4,3,8,'2018-06-23 08:37:28',NULL);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `code` varchar(150) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Quản trị hệ thống','ADMIN',NULL,NULL),(2,'người dùng','USER',NULL,NULL),(3,'Quản lý','MANAGER',NULL,NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `fullname` varchar(150) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','123456','admin',1,NULL,NULL),(2,'nguyenvana','123456','nguyen van a',1,NULL,NULL),(3,'manager1','123456','manager 1',1,NULL,NULL),(4,'manager2','123456','manager 2',1,NULL,NULL),(5,'nguyenvanb','456789','nguyen van b',1,'2018-05-29 16:32:09',NULL),(6,'manager3','123456','manager3',1,'2018-05-31 03:42:19',NULL),(7,'manager4','123456','manager4',1,'2018-05-31 03:42:44',NULL),(8,'danglehuythai','456789','dang le huy thai',1,'2018-06-05 12:16:17',NULL),(9,'quanly1','123456','quan ly 1',1,'2018-06-05 12:19:10',NULL),(10,'quanly2','123456','quan ly 2',1,'2018-06-05 12:20:39',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `roleid` bigint(20) NOT NULL,
  `createddate` timestamp NULL DEFAULT NULL,
  `modifieddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_userrole_user` (`userid`),
  KEY `fk_userrole_role` (`roleid`),
  CONSTRAINT `fk_userrole_role` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_userrole_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1,1,NULL,NULL),(3,3,3,NULL,NULL),(4,4,3,NULL,NULL),(5,5,2,NULL,NULL),(10,2,2,NULL,NULL),(11,6,3,NULL,NULL),(12,7,3,NULL,NULL),(13,8,2,NULL,NULL),(16,9,3,NULL,NULL),(17,10,3,NULL,NULL);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-13 21:31:11
