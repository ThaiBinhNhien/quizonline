package com.example.constant;

public class SystemConstant {

	public static final String MODEL = "model";
	public static final String MESSAGE_RESPONSE = "messageResponse";
	public static final String SORT_ASC = "1";
	public static final String TYPE_LIST = "list";
	public static final String TYPE_EDIT = "edit";
	public static final String TYPE_DELETE = "delete";
	public static final String LIST_ITEMS = "items";
	public static final String FOLDER_UPLOAD = "fileupload";
	public static final String SEARCH_EXAMINATION_QUESTION = "SEARCH_EXAMINATION_QUESTION";
}
