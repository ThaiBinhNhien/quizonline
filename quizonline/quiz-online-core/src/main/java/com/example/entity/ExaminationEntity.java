package com.example.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "examination")
public class ExaminationEntity extends BaseEntity {

	private static final long serialVersionUID = -2085446926586243836L;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "createdby")
	private String createdBy;

	@Column(name = "time")
	private int time;

	@Column(name = "maxiteration")
	private int maxIteration;

	@OneToMany(mappedBy = "examination", fetch = FetchType.EAGER)
	private List<ExaminationQuestionEntity> examinationQuestions;

	@OneToMany(mappedBy = "examination", fetch = FetchType.EAGER)
	private List<ResultEntity> results;

	@ManyToOne
	@JoinColumn(name = "categoryid")
	private CategoryEntity category;

	@ManyToOne
	@JoinColumn(name = "levelid")
	private LevelEntity level;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List<ExaminationQuestionEntity> getExaminationQuestions() {
		return examinationQuestions;
	}

	public void setExaminationQuestions(List<ExaminationQuestionEntity> examinationQuestions) {
		this.examinationQuestions = examinationQuestions;
	}

	public List<ResultEntity> getResults() {
		return results;
	}

	public void setResults(List<ResultEntity> results) {
		this.results = results;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getMaxIteration() {
		return maxIteration;
	}

	public void setMaxIteration(int maxIteration) {
		this.maxIteration = maxIteration;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public LevelEntity getLevel() {
		return level;
	}

	public void setLevel(LevelEntity level) {
		this.level = level;
	}
}
