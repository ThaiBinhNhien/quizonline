package com.example.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "level")
public class LevelEntity extends BaseEntity {

	private static final long serialVersionUID = 1513206241661601409L;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@OneToMany(mappedBy = "level", fetch = FetchType.EAGER)
	private List<ExaminationEntity> examinations;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<ExaminationEntity> getExaminations() {
		return examinations;
	}

	public void setExaminations(List<ExaminationEntity> examinations) {
		this.examinations = examinations;
	}
}
