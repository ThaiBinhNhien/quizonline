package com.example.entity;

/**
 * Created by Admin on 24/11/2017.
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "result")
public class ResultEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "correctnumber")
	private Integer correctNumber;

	@Column(name = "totalnumber")
	private Integer totalNumber;

	@ManyToOne
	@JoinColumn(name = "examinationid")
	private ExaminationEntity examination;

	@ManyToOne
	@JoinColumn(name = "userid")
	private UserEntity user;

	public ExaminationEntity getExamination() {
		return examination;
	}

	public void setExamination(ExaminationEntity examination) {
		this.examination = examination;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public Integer getCorrectNumber() {
		return correctNumber;
	}

	public void setCorrectNumber(Integer correctNumber) {
		this.correctNumber = correctNumber;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}
}
