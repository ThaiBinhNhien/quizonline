package com.example.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "category")
public class CategoryEntity extends BaseEntity {

	private static final long serialVersionUID = 6756233613480692732L;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@OneToMany(mappedBy = "category", fetch = FetchType.EAGER)
	private List<ExaminationEntity> examinations;
	
	@Column(name = "levels")
	private String levels;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<ExaminationEntity> getExaminations() {
		return examinations;
	}

	public void setExaminations(List<ExaminationEntity> examinations) {
		this.examinations = examinations;
	}

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}
	
}
